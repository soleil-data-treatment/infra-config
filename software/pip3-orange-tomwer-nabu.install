#!/bin/sh

set -e

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
  export http_proxy
  export https_proxy
fi

export DEBIAN_FRONTEND=noninteractive 
export DEBCONF_NONINTERACTIVE_SEEN=true

echo "$0: start Installing Orange/Tomwer (proxy=$http_proxy)"

# installation initially defined from
# https://github.com/biolab/orange3/blob/master/requirements-core.txt
# https://github.com/oasys-kit/oasys-installation-scripts/blob/master/Linux/install_oasys1.3.sh
# https://gitlab.esrf.fr/tomotools/tomwer

apt install -y -qq aptitude python3-venv
for i in python3-fabio pandoc python3-anyqt python3-bottleneck python3-chardet python3-colorcet python3-glymur python3-graypy python3-h5py python3-httpx python3-joblib python3-keyrings python3-keyrings.alt python3-lmfit python3-louvain python3-requests python3-lxml python3-networkx python3-openpyxl python3-opentsne python3-pandas python3-pebble python3-pil python3-pkg-resources python3-pyqtgraph python3-serverfiles python3-sklearn python3-spectral python3-tifffile python3-werkzeug python3-xlrd python3-yaml qt5-style-plugins python3-pyqt5 python3-pyqt5.qtwebengine python3-asteval python3-bottleneck python3-h11 python3-httpcore python3-httpx python3-jaraco.classes python3-joblib python3-pygments python3-rfc3986 python3-threadpoolctl python3-xlsxwriter libxrl-dev jupyter; do
  apt install -y -qq $i || echo "$0: WARNING: apt could not install $i"
done

# create a local venv (to be shared)
NAME=orange-tomwer-nabu
TARGET=/opt/venvs/$NAME
python3 -m venv $TARGET # --system-site-packages
. $TARGET/bin/activate
            
pip3 install --proxy=$http_proxy "tomwer[full]" jupyter

# create launcher
FILE=/usr/share/applications/$NAME.desktop
dd status=none of=${FILE} << EOF
[Desktop Entry]
Name=Orange/Nabu Tomography reconstruction
Comment=Automated reconstruction processes for Tomography
Exec=bash -c "source $TARGET/bin/activate; $TARGET/bin/tomwer canvas"
Terminal=true
Type=Application
Categories=Development;
StartupNotify=true
EOF

# create launcher
FILE=/usr/share/applications/nabu-jupyter.desktop
dd status=none of=${FILE} << EOF
[Desktop Entry]
Name=Nabu tomography reconstruction (Jupyter)
Comment=Nabu tomography reconstruction Jupyter Notebook
Exec=bash -c "source $TARGET/bin/activate; python3 -m ipykernel install --user --name=$NAME; jupyter notebook"
Terminal=true
Type=Application
Categories=Development;Graphics;Education;
StartupNotify=true
EOF

# create launcher
FILE=/usr/share/applications/nabu-terminal.desktop
dd status=none of=${FILE} << EOF
[Desktop Entry]
Name=Nabu tomography reconstruction (Terminal)
Comment=Nabu Terminal
Exec=bash -c "source $TARGET/bin/activate; exec bash"
Terminal=true
Type=Application
Categories=Development;Graphics;Education;
StartupNotify=true
EOF

find $TARGET -type d -exec chmod 777 {} \;
find $TARGET -type f -exec chmod a+r {} \;

echo "$0: Done"

