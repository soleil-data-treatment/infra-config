#!/bin/sh

# Script for use with DARTS/qemu-web-desktop
# https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop
#   It is launched when a new session starts.
# The following symbols are replaced before execution:
#   @USER@ @VM@ @SESSION_NAME@ and @PW@
#
# Specify in /etc/qemu-web-desktop/config.pl
# my @config_scripts=('if("@VM@" =~ /debian/i): https://gitlab.com/soleil-data-treatment/infra-config/-/raw/master/hosts/grades-vm/SOLEIL-DARTS-auto-login.sh');
# $config{config_script}            = [@config_scripts];

# create local copy of script in /tmp
# r=`realpath $0`
# cp $r /tmp

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
  export http_proxy
  export https_proxy
fi

# wait for network and LDAP to be ready
sleep 2

# get information from the LDAP: user exists ?

LDAP=ldap://195.221.10.1
USERFILE=/tmp/user.txt
user=@USER@
s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "mail=$user"`

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "cn=$user"`
fi

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "uid=$user"`
fi

# analyze the LDAP entry
echo "== Welcome to DARTS ! ==" > $USERFILE
date >> $USERFILE
echo " _____          _____ _______ _____ " >> $USERFILE
echo "|  __ \   /\   |  __ \__   __/ ____|" >> $USERFILE
echo "| |  | | /  \  | |__) | | | | (___  " >> $USERFILE
echo "| |  | |/ /\ \ |  _  /  | |  \___ \ " >> $USERFILE
echo "| |__| / ____ \| | \ \  | |  ____) |" >> $USERFILE
echo "|_____/_/    \_\_|  \_\ |_| |_____/ " >> $USERFILE
echo "https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop" >> $USERFILE
echo " " >> $USERFILE
echo "(c) Synchrotron SOLEIL / EXP / GRADES" >> $USERFILE
echo "https://data-analysis.synchrotron-soleil.fr" >> $USERFILE
echo "HELP: synchrotron-soleil-data-treatment@groupes.renater.fr" >> $USERFILE
echo " " >> $USERFILE
echo "-----------------------------------------------------" >> $USERFILE
echo "system=@VM@" >> $USERFILE
echo "user=$user" >> $USERFILE

shel=`echo $s | grep -o "loginShell: .* .*" | awk '{ print $2 }'`
if [ x$shel = 'x/dev/null'  -o x$shel = 'x' ]; then
  can_login=0;
else
  can_login=1;
  echo "canLogin=$can_login" >> $USERFILE
fi

# get email
email=`echo $s | grep -o "mail: .*" | awk '{ print $2 }'`
c=`echo $email | grep -o "@"`
if [ -z $c ]; then
  has_email=0;
else
  has_email=1;
  echo "email=$email" >> $USERFILE
fi

# get uid
uid=`echo $s | grep -o "uidNumber: .*" | awk '{ print $2 }'`
echo "uidNumber=$uid" >> $USERFILE

# get gid
gid=`echo $s | grep -o "gidNumber: .*" | awk '{ print $2 }'`
echo "gidNumber=$gid" >> $USERFILE

# get homedir
homedir=`echo $s | grep -o "homeDirectory: .*" | awk '{ print $2 }'`
echo "homeDirectory=$homedir" >> $USERFILE
echo "-----------------------------------------------------" >> $USERFILE
echo " " >> $USERFILE

echo "DEBUG: Welcome $user"
cat $USERFILE

# we unactivate ntp server search which is blocked by our proxy
echo "DEBUG: Disabling NTP (blocked by proxy)"
timedatectl set-ntp false
systemctl disable --now systemd-timesyncd
systemctl disable --now ntp

# ------------------------------------------------------------------------------

# switch auto-login to USER if can_login and has_email, else remain as guest
# if [ $can_login = 1 -a $has_email = 1 ]; then
if [ $can_login = 1 ]; then

  # make sure account exists, from LDAP
  echo "DEBUG: Creating user $user"
  # create a user with proper uid/gid and local account
  # we store the PW into a locked file to avoid issue with special chars
  PWFILE=`mktemp -p /tmp`
  dd status=none of=${PWFILE} << 'EOF'
@PW@
EOF
   
  # the following line may fail when PW contains ' or "
  pass=$(perl -e 'print crypt($ARGV[0], "@PW@")' $password)
  /sbin/addgroup --gid $gid group_$user
  /sbin/useradd -m -d $homedir $user -s /usr/bin/bash -u $uid -g $gid
  cat $PWFILE | passwd $user
  
  chown $user $PWFILE
  
  # add Docker rights
  /sbin/groupadd docker
  /sbin/usermod -aG docker $user
  mkdir mkdir -p /etc/systemd/system/docker.service.d
  FILE=/etc/systemd/system/docker.service.d/http-proxy.conf
  dd status=none of=${FILE} << EOF
[Service]
Environment="HTTP_PROXY=$http_proxy"
Environment="HTTPS_PROXY=$http_proxy"
EOF
  systemctl daemon-reload
  systemctl restart docker
  FILE=/tmp/config.json
  dd status=none of=${FILE} << EOF
{
 "proxies":
  {
    "default":
    {
      "httpProxy":"$http_proxy",
      "httpsProxy":"$http_proxy"
    }
  }
}
EOF
  su -c "mkdir -p ~/.docker/ ; cp /tmp/config.json ~/.docker/" $user

  # map /mnt/home to /home on host (allow Jupyter interaction)
  /sbin/iptables -I OUTPUT -d 10.0.2.2        -p tcp --dport ssh -j ACCEPT
  su -c "mkdir -p $homedir/.ssh ; ssh-keygen -f $homedir/.ssh/known_hosts -R \"10.0.2.2\"" $user
  
  su -c "mkdir -p /tmp/sshfs-home-host ; cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no 10.0.2.2:/home/ /tmp/sshfs-home-host" $user
  ln -s /tmp/sshfs-home-host /mnt/home
  # mount -t 9p -o trans=virtio,msize=104857600,rw host_home /mnt/home || echo "ERROR/FAILED: mount 9p /mnt/home"
  
  # add apptainer area in /opt
  ln -s /mnt/home/software/apptainer /opt/apptainer

else
  user=guest
fi # can_login

# ------------------------------------------------------------------------------
# common settings for both guest and USER

echo "DEBUG: Setting background, kb and panel"
# add VM name to desktop background image
BKG=/usr/share/backgrounds/desktop-background-SOLEIL.png
convert -pointsize 30 -fill '#0454a7' \
        -draw 'text 150,1000 "DARTS @VM@"' \
        -pointsize 30 -fill black \
        -draw 'text 300,50 "Save your data (persistent area, Ruche...). Issues ? Send email to helpinfo@synchrotron-soleil.fr"' \
        $BKG \
        $BKG || echo "WARNING: Could not find background $BKG"
su -c 'gsettings set org.cinnamon.desktop.background picture-uri "file:///usr/share/backgrounds/desktop-background-SOLEIL.png"; exit' $user
su -c 'gsettings set org.cinnamon.desktop.background picture-options "stretched"; exit' $user 

su -c "gsettings set org.gnome.libgnomekbd.keyboard layouts \"['fr','fr\tmac','us','us\tmac']\"; exit" $user
su -c "gsettings set org.cinnamon.desktop.interface toolkit-accessibility true; exit" $user
su -c "gsettings set org.cinnamon.desktop.a11y.keyboard enable true; exit" $user
# su -c "gsettings set org.cinnamon.settings-daemon.peripherals.keyboard repeat false; exit" $user
# su -c "gsettings set org.gnome.desktop.a11y.applications screen-keyboard-enabled true; exit" $user
su -c "gsettings set org.cinnamon enabled-applets \"['panel1:left:0:menu@cinnamon.org:0', 'panel1:left:1:show-desktop@cinnamon.org:1', 'panel1:left:2:grouped-window-list@cinnamon.org:2', 'panel1:right:0:systray@cinnamon.org:3', 'panel1:right:1:xapp-status@cinnamon.org:4', 'panel1:right:2:notifications@cinnamon.org:5', 'panel1:right:3:printers@cinnamon.org:6', 'panel1:right:4:removable-drives@cinnamon.org:7', 'panel1:right:6:favorites@cinnamon.org:9', 'panel1:right:7:network@cinnamon.org:10', 'panel1:right:8:sound@cinnamon.org:11', 'panel1:right:9:power@cinnamon.org:12', 'panel1:right:10:calendar@cinnamon.org:13', 'panel1:right:11:a11y@cinnamon.org:14', 'panel1:right:12:keyboard@cinnamon.org:15', 'panel1:right:14:trash@cinnamon.org:17']\"" $user
# disable screen lock
su -c "gsettings set org.cinnamon.desktop.screensaver lock-enabled false; exit" $user
su -c "gsettings set org.cinnamon.desktop.lockdown disable-lock-screen true; exit" $user
su -c "cp /usr/share/applications/cinnamon-display-panel.desktop ~/Desktop; chmod a+x ~/Desktop/cinnamon-display-panel.desktop; exit" $user

# create an autostart in ~/.config/autostart/
if [ -f "/usr/bin/xkb-switch" ]; then
  FILE_KB=/tmp/kb_fs.desktop
  echo "[Desktop Entry]" >> $FILE_KB
  echo "Type=Application" >> $FILE_KB
  echo "Exec=xkb-switch -s fr" >> $FILE_KB
  echo "X-GNOME-Autostart-enabled=true" >> $FILE_KB
  echo "NoDisplay=false" >> $FILE_KB
  echo "Hidden=false" >> $FILE_KB
  echo "Name[C]=kb_fr" >> $FILE_KB
  echo "Comment[C]=Set French KB" >> $FILE_KB
  echo "X-GNOME-Autostart-Delay=2" >> $FILE_KB
  su -c "mkdir -p ~/.config/autostart; cp $FILE_KB ~/.config/autostart/; exit" $user
  rm $FILE_KB
fi

if [ -f "/usr/bin/xrandr" ]; then
  FILE_XRANDR=/tmp/xrandr.desktop
  echo "[Desktop Entry]" >> $FILE_XRANDR
  echo "Type=Application" >> $FILE_XRANDR
  echo "Exec=xrandr -s 1680x1050" >> $FILE_XRANDR
  echo "X-GNOME-Autostart-enabled=true" >> $FILE_XRANDR
  echo "NoDisplay=false" >> $FILE_XRANDR
  echo "Hidden=false" >> $FILE_XRANDR
  echo "Name[C]=xrandr" >> $FILE_XRANDR
  echo "Comment[C]=Set screen size" >> $FILE_XRANDR
  echo "X-GNOME-Autostart-Delay=1" >> $FILE_XRANDR
  su -c "mkdir -p ~/.config/autostart; cp $FILE_XRANDR ~/.config/autostart/; exit" $user
  rm $FILE_XRANDR
fi

# set Firefox proxy to 0=none; 1=manual; 2=auto pac; 4=auto; 5=system.
echo 'pref("network.proxy.type", 5);' >> /etc/firefox-esr/soleil-prefs.js

# ------------------------------------------------------------------------------
# mounts and auto-login

# set auto-login
# take into account the new auto-login with restart
echo "DEBUG: Setting auto-login for $user, restart desktop"

s=`systemctl status lightdm | grep -o Active`
if [ -z "$s" ]; then
  echo "[daemon]"                   > /etc/gdm3/custom.conf
  echo "AutomaticLoginEnable=true" >> /etc/gdm3/custom.conf
  echo "AutomaticLogin=$user"     >> /etc/gdm3/custom.conf
  echo "TimedLoginEnable=true"     >> /etc/gdm3/custom.conf
  echo "TimedLogin=$user"         >> /etc/gdm3/custom.conf
  echo "TimedLoginDelay=0"         >> /etc/gdm3/custom.conf
  sleep 1
  systemctl restart gdm3.service
else
  if [ -d "/etc/lightdm/lightdm.conf.d" ]; then
    FILE=/etc/lightdm/lightdm.conf.d/12-autologin.conf
    echo "[SeatDefaults]"          > $FILE
  else
    FILE=/etc/lightdm/lightdm.conf
    echo "[SeatDefaults]"         >> $FILE
  fi

  echo "autologin-user=$user"    >> $FILE
  echo "autologin-user-timeout=0" >> $FILE
  sleep 1
  systemctl restart lightdm.service
  sleep 1
  # set French kb and screen size for $user
  # su -c "xrandr -s 1440x900; xkb-switch -s fr; exit" $user
fi
  
if [ $can_login = 1 ]; then
  # set auto-login
  # take into account the new auto-login with restart
  echo "DEBUG: Mount persistent area $HOME"

  # mount raw data and JupyterHub persistent area. /mnt/home -> sshfs host:/home is set when 'can_login'
  persistent_exists=`su -c "if [ -d /mnt/$HOME ]; then echo 1; fi" $user`
  if [ $persistent_exists = 1 ]; then
    echo "DEBUG: Add $homedir/Desktop/persistent_area"
    su -c "mkdir -p /tmp/sshfs-persistent_area; cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no 10.0.2.2:$homedir /tmp/sshfs-persistent_area" $user
    
    su -c "ln -s /tmp/sshfs-persistent_area \$HOME/Desktop/persistent_area ; ln -s /tmp/sshfs-persistent_area \$HOME/persistent_area; echo 'PATH=\$PATH:/tmp/sshfs-persistent_area/bin' >> ~/.bashrc" $user
    ln -s /tmp/sshfs-persistent_area /mnt/persistent_area
    echo "The persistent area is at: \$HOME/persistent_area (persistent GRADES, shared with the JupyterHub)" >> $USERFILE
  fi

  # Ruche mount
  echo "DEBUG: Mount $homedir/Desktop/ruche"
  su -c "mkdir -p /tmp/sshfs-ruche; cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no 10.0.2.2:/nfs/ruche /tmp/sshfs-ruche" $user
  su -c "ln -s /tmp/sshfs-ruche \$HOME/Desktop/ruche ; ln -s /tmp/sshfs-ruche \$HOME/Desktop/ruche; ln -s /tmp/sshfs-ruche \$HOME/ruche" $user
  mkdir -p /nfs
  ln -s /tmp/sshfs-ruche /nfs/ruche-sshfs
  ln -s /tmp/sshfs-ruche /mnt/ruche

  if [ -d "/nfs/ruche" ]; then
    # replace Ruche mount by the user sshfs mount which is more stable
    rm /nfs/ruche
  fi
  ln -s /nfs/ruche-sshfs /nfs/ruche

  RUCHEHOME=`echo "$homedir" | sed -e 's|/home|ruche|g'`
  echo "The Ruche is at:           \$HOME/ruche (for storing files permanently)" >> $USERFILE
  echo "  Your Ruche 'home' is at: \$HOME/$RUCHEHOME (your personnal persistent area Ruche)" >> $USERFILE
  echo "  The temporary area is:   \$HOME/ruche/share-temp (to exchange files)" >> $USERFILE

  # additional mounts for e.g. WORK / VISA
  # map /mnt/work-lustre to the one on host (Lustre re-lexport-01:/work)
#  echo "DEBUG: Mount ISI work"
#  su -c "mkdir -p /tmp/sshfs-work-lustre ; cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no 10.0.2.2:/mnt/work-re-lexport-01/ /tmp/sshfs-work-lustre" $user &
#  su -c "ln -s /tmp/sshfs-work-lustre \$HOME/Desktop/work ; ln -s /tmp/sshfs-work-lustre \$HOME/Desktop/work; ln -s /tmp/sshfs-work-lustre \$HOME/work" $user
#  ln -s /tmp/sshfs-work-lustre /mnt/work-lustre
#  ln -s /tmp/sshfs-work-lustre /nfs/work
#  echo "The ISI work area is at:   /mnt/work-lustre (Lustre, shared with SUMO cluster)" >> $USERFILE

  rm $PWFILE
  
  # final copy of README and bookmarks
  echo "DEBUG: Copying bookmarks and README"
  echo " " >> $USERFILE
  if [ -f $homedir/Desktop/START-HERE/README ]; then
    cat $USERFILE $homedir/Desktop/START-HERE/README > /tmp/README
    su -c "cp /tmp/README \$HOME/Desktop/START-HERE/" $user
    rm /tmp/README $USERFILE
  else
    su -c "mkdir -p \$HOME/Desktop/START-HERE/; cp $USERFILE \$HOME/Desktop/START-HERE/" $user
  fi

  # add bookmarks
  su -c "mkdir -p \$HOME/.config/gtk-3.0; touch \$HOME/.config/gtk-3.0/bookmarks" $user
  for DIR in /tmp/sshfs-persistent_area /tmp/sshfs-home-host /tmp/sshfs-ruche /tmp/sshfs-work-lustre ; do
    su -c "grep -qxF \"file://$DIR\" \$HOME/.config/gtk-3.0/bookmarks || echo \"file://$DIR\" >> \$HOME/.config/gtk-3.0/bookmarks" $user
  done


fi # can_login

# add firewall rules (/sbin//iptables) to restrict access to SOLEIL infrastructure machines
echo "DEBUG: Setting firewall"
# allow SSH access to QEMU host (for sshfs to persistent area and /nfs/ruche)
#/sbin/iptables -I OUTPUT -d 195.221.10.46   -p tcp --dport ssh -j ACCEPT
#/sbin/iptables -I OUTPUT -d 195.221.10.56   -p tcp --dport ssh -j ACCEPT
#/sbin/iptables -I OUTPUT -d $SUMO   -p tcp --dport ssh -j ACCEPT

# restrict SSH (append at end -A) to all infrastructure servers
/sbin/iptables -A OUTPUT -d 195.221.0.0/16  -p tcp --dport ssh -j REJECT
/sbin/iptables -A OUTPUT -d 172.28.0.0/16   -p tcp --dport ssh -j REJECT
/sbin/iptables -A OUTPUT -d 192.168.0.0/16  -p tcp --dport ssh -j REJECT
# /sbin/iptables -A OUTPUT -d 10.0.0.0/8      -p tcp --dport ssh -j REJECT

echo "DEBUG: Getting boot logs"
# final touch with software and config
if [ -x /usr/bin/ipython3 ]; then
  ln -s /usr/bin/ipython3 /usr/local/bin/ipython            || echo "WARNING: Could not alias ipython3"
fi

# save boot log for further analysis
mkdir -p /opt/infra-config
journalctl -b --no-pager > /opt/infra-config/journalctl.log || echo "WARNING: Could not get journalctl -b"
dmesg                    > /opt/infra-config/dmesg.log      || echo "WARNING: Could not get dmesg"
if [ -f /var/log/syslog ]; then
  cp /var/log/syslog /opt/infra-config/syslog.log           || echo "WARNING: Could not get syslog"
fi
chmod a+r /opt/infra-config/*.log

# disable root login
/sbin/usermod -s /sbin/nologin root
