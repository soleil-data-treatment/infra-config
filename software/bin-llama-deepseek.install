#!/bin/bash

# install llama.cpp
# grab Mistral and Deepseek in 14B versions
# create executables for deepseek
# create web service

# see REF: https://pyimagesearch.com/2024/08/26/llama-cpp-the-ultimate-guide-to-efficient-llm-inference-and-applications/

set -e

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
  export http_proxy
  export https_proxy
fi

echo "$0: start"

NAME=llama.cpp
SHARE_TARGET=/usr/share/$NAME
BIN_TARGET=/usr/bin
LIB_TARGET=/usr/lib

# BUILD:
# ------------------------------------------------------------------------------
apt install libcurl4-openssl-dev cmake build-essential

echo "$0: compile"
cd /tmp
git clone https://github.com/ggerganov/llama.cpp.git
cd llama.cpp/
cmake -S . -B build -LH -DGGML_CUDA=ON -DLLAMA_CURL=ON
cmake --build build --config Release -j

echo "$0: install into $BIN_TARGET/ $LIB_TARGET/ and $SHARE_TARGET/"
cd build/bin
# we only install the cli and the server
cp llama-cli    $BIN_TARGET/
cp llama-server $BIN_TARGET/
for i in lib*; do cp $i $LIB_TARGET/$i; done
cd ../..
mkdir -p $SHARE_TARGET
cp -r models prompts $SHARE_TARGET/

echo "$0: Get and install models"
# DeepSeek-R1 multilingual 14b
REPO=NikolayKozloff/DeepSeek-R1-Distill-Qwen-14B-Multilingual-Q5_K_S-GGUF
MODEL=deepseek-r1-distill-qwen-14b-multilingual-q5_k_s.gguf
cd /tmp
echo "$0: https://huggingface.co/$REPO/resolve/main/$MODEL"
wget https://huggingface.co/$REPO/resolve/main/$MODEL
mv $MODEL $SHARE_TARGET/models/

# Mistral AI 7B text
# REPO=TheBloke/Mistral-7B-Instruct-v0.2-GGUF
# MODEL=mistral-7b-instruct-v0.2.Q8_0.gguf

# DeepSeek Janus Pro  text-to-image
# REPO=mradermacher/Janus-Pro-7B-LM-GGUF
# MODEL=Janus-Pro-7B-LM.Q8_0.gguf

echo "$0: Install SOLEIL prompt"
FILE=$SHARE_TARGET/prompts/chat-soleil.txt
dd status=none of=${FILE} << EOF
User: Bonjour.
AI: Bonjour je suis un LLM DeepSeek local pour SOLEIL. Je ne transmets aucune informations sur Internet. Nos conversations resteront privées et ne sont pas conservées.
User: OK.
AI: Je parle de nombreuses langues. De quoi voulez-vous parler ?
User:
EOF

echo "$0: Install SOLEIL chat launcher"
FILE=$BIN_TARGET/deepseek
dd status=none of=${FILE} << EOF
#!/bin/sh

# usage: deepseek [args]
#
# all passed args are given to llama.cpp
#
# models  are in $SHARE_TARGET/models
# prompts are in $SHARE_TARGET/prompts

MODEL=$SHARE_TARGET/$MODEL
PROMPT=/usr/share/llama.cpp/prompts/chat-soleil.txt

# make sure we do not connect outside
export HTTP_PROXY= 
export HTTPS_PROXY= 
export http_proxy= 
export https_proxy= 

llama-cli --model \$MODEL \
    --ctx_size 2048 -n -1 -b 256 -cnv \
    --temp 0.8 --repeat_penalty 1.1 \
    --color -r "User:" --in-prefix " " -i \
    -ngl 128 -f \$PROMPT \$@
EOF
chmod a+x $FILE

echo "$0: Install SOLEIL server launcher"
FILE=$BIN_TARGET/deepseek-server
dd status=none of=${FILE} << EOF
#!/bin/sh

# usage: deepseek-server [args]
#
# all passed args are given to llama.cpp
#
# models  are in $SHARE_TARGET/models

MODEL=$SHARE_TARGET/$MODEL
PORT=8501
HOST=0.0.0.0

# make sure we do not connect outside
export HTTP_PROXY= 
export HTTPS_PROXY= 
export http_proxy= 
export https_proxy= 

# 4 simultaneous sessions with 128 GPU off-loaded layers. Context 2048 tokens.
llama-server --model \$MODEL \
    --ctx_size 2048 -n -1 -b 256 -np 4 \
    --temp 0.8 --repeat_penalty 1.1 -ngl 128 \
    --port \$PORT --host \$HOST \$@
EOF
chmod a+x $FILE

echo "$0: Create web service"
FILE=/lib/systemd/system/llama-deepseek.service
dd status=none of=${FILE} << EOF
[Unit]
Description=LLaMa.CPP DeepSeek web service
After=syslog.target network.target remote-fs.target
 
StartLimitIntervalSec=500
StartLimitBurst=5
 
[Service]
Restart=on-failure
RestartSec=5s
User=farhie
ExecStart=deepseek-server
 
 
[Install]
WantedBy=multi-user.target
EOF

# activate service
systemctl daemon-reload
systemctl enable llama-deepseek
systemctl start  llama-deepseek

echo "$0: Done"
