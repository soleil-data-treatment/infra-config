{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings         #-}

module Hosts where

import           Data.Maybe
import           Data.Text
import           Data.Text.IO
import           System.Directory
import           System.FilePath

import           Prelude          hiding (unlines, writeFile)

type Backports = Text
type Comment = Text
type Flavour = Text
newtype HostName = HostName Text
type Ip = Text
type Port = Text
type Install = Text
type Mode = Text
type Package = Text
type Script = [Text]
type Service = Text
type Sources = [Text]
type Uri = Text
type Url = Text
type User = Text
type Version = Text

data Beamline
  = Sixs

data BuildType
  = Local
  | Shm FilePath

data CheckHost
  = CheckHost HostName

data CheckRoot
  = CheckRoot

data Distribution
  = Stable Text
  | Testing
  | Upgrade Distribution Distribution

data Echo
  = Echo Text

data Exim4
  = Exim4

data FileContent
  = FileContent Text (Maybe Mode) [Text]

data Firefox
  = Firefox (Maybe Network)

data Host
  = Host HostName Distribution (Maybe Network) [Scriptable]

data InstallPkgs
  = InstallPkgs [Package]

data Ldap
  = Ldap Uri

data Mount
  = Mount Text Text Text Text

data Network
  = Rel
  | Res

data Ntp
  = Ntp Distribution (Maybe Network)

data Proxy
  = Proxy Comment Ip Port

data Raw
  = Raw Text

data Rm
  = Rm Text Text

data RmCruft = RmCruft

data Ruche
  = Ruche Beamline

data Ssh = Ssh

data Sudoers
  = Sudoers [User]

data UnitName
  = UnitName Text
  | UnitName'FromPath Text

data SystemD
  = SystemCtl SystemDAction
  | SystemDUnit UnitName [Text]

data SystemDAction
  = SDDaemonReload
  | SDEnable Service
  | SDMask Service
  | SDStart Service
  | SDStop Service
  | SDRestart Service

data Timezone
  = Timezone Text

data Wm
  = Gnome

data Scriptable = forall a . ToScript a => MkScriptable a

script :: ToScript a => a -> Scriptable
script = MkScriptable

class ToScript a where
  toScript ::  a -> Script

instance ToScript Scriptable where
  toScript (MkScriptable a) = toScript a

instance ToScript Text where
  toScript t = [t]

instance ToScript a => ToScript (Maybe a) where
  toScript Nothing  = []
  toScript (Just a) = toScript a

instance ToScript a => ToScript [a] where
  toScript as = Prelude.concatMap toScript as

instance ToScript CheckHost where
  toScript (CheckHost (HostName h))
    = toScript $ Raw ("if [[ \"$(hostname --fqdn)\" != \"" <> h <> "\" ]]; then echo \"Wrong  machine existing\"; exit 1; fi")

instance ToScript CheckRoot where
  toScript CheckRoot
    = toScript $ Raw "if [ \"$(id -u)\" -ne 0 ] ; then echo \"$0: Please run as root\" ; exit 1 ; fi"

sourceList :: Distribution -> FileContent
sourceList d = FileContent "/etc/apt/sources.list" Nothing (content d)
  where
    content :: Distribution -> [Text]
    content (Stable "bullseye") = [ "deb http://deb.debian.org/debian bullseye main contrib non-free"
                                  , "deb http://deb.debian.org/debian bullseye-updates main contrib non-free"
                                  , "deb http://security.debian.org/debian-security bullseye-security main contrib non-free"
                                  , "deb http://deb.debian.org/debian bullseye-backports main contrib non-free"
                                  ]
    content (Stable release) = [ "deb http://deb.debian.org/debian " <> release <> " main contrib non-free non-free-firmware"
                               , "deb http://deb.debian.org/debian " <> release <> "-updates main contrib non-free non-free-firmware"
                               , "deb http://security.debian.org/debian-security " <> release <> "-security main contrib non-free non-free-firmware"
                               , "deb http://deb.debian.org/debian " <> release <> "-backports main contrib non-free non-free-firmware"
                               ]
    content Testing = [ "deb http://deb.debian.org/debian testing main contrib non-free non-free-firmware" ]
    content (Upgrade _ t) = content t


instance ToScript Distribution where
  toScript distribution
    = toScript [ script install ]
    where
      install = case distribution of
                  Stable _    -> [ script $ sourceList distribution
                                , script upgrade ]
                  Testing     -> [ script $ sourceList distribution
                                , script upgrade ]
                  Upgrade f t -> [ script $ sourceList f
                                , script upgrade
                                , script $ sourceList t
                                , script upgrade ]

      upgrade =  [ script $ Raw "export DEBIAN_FRONTEND=noninteractive"
                 , script $ Raw "apt update"
                 , script $ Raw "apt -y upgrade"
                 , script $ Raw "apt -y dist-upgrade"
                 , script $ Raw "apt -y autoremove --purge"
                 ]


instance ToScript Echo where
  toScript (Echo t) = toScript $ Raw ("echo " <> t)


instance ToScript Exim4 where
  toScript Exim4
    = toScript [ script install
               , script configure
               , script reload
               ]
    where
      install = InstallPkgs [ "exim4" ]
      configure = [ FileContent "/etc/exim4/conf.d/rewrite/00_exim4-config_header" Nothing
                    [ "######################################################################"
                    , "#                      REWRITE CONFIGURATION                         #"
                    , "######################################################################"
                    , ""
                    , "begin rewrite"
                    , ""
                    , "root@* picca@synchrotron-soleil.fr FfrsTtcb"
                    ]
                  , FileContent "/etc/exim4/update-exim4.conf.conf" Nothing
                    [ "# /etc/exim4/update-exim4.conf.conf"
                    , "#"
                    , "# Edit this file and /etc/mailname by hand and execute update-exim4.conf"
                    , "# yourself or use 'dpkg-reconfigure exim4-config'"
                    , "#"
                    , "# Please note that this is _not_ a dpkg-conffile and that automatic changes"
                    , "# to this file might happen. The code handling this will honor your local"
                    , "# changes, so this is usually fine, but will break local schemes that mess"
                    , "# around with multiple versions of the file."
                    , "#"
                    , "# update-exim4.conf uses this file to determine variable values to generate"
                    , "# exim configuration macros for the configuration file."
                    , "#"
                    , "# Most settings found in here do have corresponding questions in the"
                    , "# Debconf configuration, but not all of them."
                    , "#"
                    , "# This is a Debian specific file"
                    , ""
                    , "dc_eximconfig_configtype='satellite'"
                    , "dc_other_hostnames='synchrotron-soleil.fr'"
                    , "dc_local_interfaces='127.0.0.1'"
                    , "dc_readhost='synchrotron-soleil.fr'"
                    , "dc_relay_domains=''"
                    , "dc_minimaldns='false'"
                    , "dc_relay_nets=''"
                    , "dc_smarthost='smtp.synchrotron-soleil.fr'"
                    , "CFILEMODE='644'"
                    , "dc_use_split_config='true'"
                    , "dc_hide_mailname='true'"
                    , "dc_mailname_in_oh='true'"
                    , "dc_localdelivery='mail_spool'"
                    ]
                  ]
      reload = SystemCtl (SDRestart "exim4")


instance ToScript FileContent where
  toScript (FileContent f mm ls)
    = toScript $ [ script $ Raw ("FILE=" <> f)
                 , script $ Echo "Writting \"${FILE}\" with content"
                 , script $ Raw "dd status=none of=${FILE} << EOF"
                 , script $ ls
                 , script $ Raw "EOF"
                 ] <> case mm of
                        Nothing   -> []
                        Just mode -> [ script $ Raw $ "chmod " <> mode <> " " <> f ]

instance ToScript Firefox where
  toScript (Firefox mr)
    = toScript [ script install
               , script configure
               ]
    where
      install = InstallPkgs [ "firefox-esr" ]
      configure = case mr of
                    Nothing -> []
                    Just r -> [ FileContent "/etc/firefox-esr/soleil-prefs.js" Nothing
                               [ "pref(\"network.proxy.backup.ftp\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.backup.ftp_port\", " <> port <> ");"
                               , "pref(\"network.proxy.backup.socks\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.backup.socks_port\", " <> port <> ");"
                               , "pref(\"network.proxy.backup.ssl\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.backup.ssl_port\", " <> port <> ");"
                               , "pref(\"network.proxy.ftp\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.ftp_port\", " <> port <> ");"
                               , "pref(\"network.proxy.http\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.http_port\", " <> port <> ");"
                               , "pref(\"network.proxy.share_proxy_settings\", true);"
                               , "pref(\"network.proxy.socks\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.socks_port\", " <> port <> ");"
                               , "pref(\"network.proxy.ssl\", \"" <> ip <> "\");"
                               , "pref(\"network.proxy.ssl_port\", " <> port <> ");"
                               , "// https://kb.mozillazine.org/Network.proxy.type"
                               , "// proxy.type=1 (system) 4 (auto)"
                               , "pref(\"network.proxy.type\", 4);"
                               ]
                             ]
                      where
                        (Proxy _ ip port) = getProxy r

instance ToScript Host where
  toScript (Host h d mr s)
    = toScript [ script CheckRoot
               , script $ CheckHost h
               , script h
               , script mr
               , script d
               , script s
               ]

instance ToScript HostName where
  toScript (HostName n)
    = toScript [ script $ Echo ("Setting the hostname: \"" <> n <> "\"")
               , script $ Raw ("hostnamectl set-hostname " <> n)
               ]

instance ToScript InstallPkgs where
  toScript (InstallPkgs ps)
    = toScript [ script $ Raw "export DEBIAN_FRONTEND=noninteractive"
               , script $ Raw ("apt install -y " <> Data.Text.unwords ps)
               ]

instance ToScript Ldap where
  toScript (Ldap uri)
    = toScript [ script install
               , script conf
               , script reload
               ]
    where
      install = InstallPkgs [ "libnss-ldapd"
                            , "debconf"
                            , "adduser"
                            , "nslcd"
                            , "libpam-ldapd"
                            , "libldap-common"
                            ]

      conf = [ FileContent "/etc/nsswitch.conf" Nothing
               [ "# DO NOT EDIT, MANAGED BY GRADES"
               , "#"
               , "# /etc/nsswitch.conf"
               , "#"
               , "# Example configuration of GNU Name Service Switch functionality."
               , "# If you have the glibc-doc-reference and info packages installed, try:"
               , "# info libc \"Name Service Switch\" for information about this file."
               , ""
               , "passwd:         files systemd ldap"
               , "group:          files systemd ldap"
               , "shadow:         files systemd ldap"
               , "gshadow:        files systemd ldap"
               , ""
               , "hosts:          files mdns4_minimal [NOTFOUND=return] dns myhostname"
               , "networks:       files"
               , ""
               , "protocols:      db files"
               , "services:       db files"
               , "ethers:         db files"
               , "rpc:            db files"
               , ""
               , "netgroup:       nis"
               ]
             , FileContent "/etc/nslcd.conf" Nothing
               [ "# DO NOT EDIT, MANAGED BY GRADES"
               , "#"
               , "# /etc/nslcd.conf"
               , "# nslcd configuration file. See nslcd.conf(5)"
               , "# for details."
               , ""
               , "# The user and group nslcd should run as."
               , "uid nslcd"
               , "gid nslcd"
               , ""
               , "# The location at which the LDAP server(s) should be reachable."
               , "uri " <> uri
               , ""
               , "# The search base that will be used for all queries."
               , "base dc=EXP"
               , ""
               , "# The LDAP protocol version to use."
               , "#ldap_version 3"
               , ""
               , "# The DN to bind with for normal lookups."
               , "#binddn cn=annonymous,dc=example,dc=net"
               , "#bindpw secret"
               , ""
               , "# The DN used for password modifications by root."
               , "#rootpwmoddn cn=admin,dc=example,dc=com"
               , ""
               , "# SSL options"
               , "#ssl off"
               , "#tls_reqcert never"
               , "tls_cacertfile /etc/ssl/certs/ca-certificates.crt"
               , ""
               , "# The search scope."
               , "#scope sub"
               , ""
               , "# SOLEIL specific in order to avoid login with System User for everyone."
               , "map passwd gecos givenName"
               ]
             , FileContent "/etc/ldap/ldap.conf" Nothing
               [ "# DO NOT EDIT, MANAGED BY GRADES"
               , "#"
               , "# LDAP Defaults"
               , "#"
               , ""
               , "# See ldap.conf(5) for details"
               , "# This file should be world readable but not world writable."
               , ""
               , "BASE   dc=EXP"
               , "URI    ${LDAP_URI}"
               , ""
               , "#SIZELIMIT      12"
               , "#TIMELIMIT      15"
               , "#DEREF          never"
               , ""
               , "# TLS certificates (needed for GnuTLS)"
               , "TLS_CACERT      /etc/ssl/certs/ca-certificates.crt"
               ]
             ]

      reload = [ script $ SystemCtl (SDRestart "nslcd")
               , script $ Raw "pam-auth-update --enable mkhomedir"
               ]

fromUnitName :: UnitName -> Text
fromUnitName (UnitName t)          = t
fromUnitName (UnitName'FromPath p) = "$(systemd-escape --path \"" <> p <> "\")"

instance ToScript Mount where
  toScript (Mount wa we t o)
    = let mount = UnitName'FromPath (we <> ".mount")
          automount = UnitName'FromPath (we <> ".automount")
      in
        toScript [ script $ SystemDUnit mount
                   [ "[Mount]"
                   , "What=" <> wa
                   , "Where=" <> we
                   , "Options=" <> o
                   , "Type=" <> t
                   ]
                 , script $ SystemDUnit automount
                   [ "[Automount]"
                   , "Where=" <> we
                   , "[Install]"
                   , "WantedBy=multi-user.target"
                   ]
                 , script $ SystemCtl SDDaemonReload
                 , script $ SystemCtl (SDEnable ("--now " <> fromUnitName automount ))
                 ]

  -- mount-point.mount
  -- [Mount]
  -- What=/dev/disk/by-uuid/f5755511-a714-44c1-a123-cfde0e4ac688
  -- Where=/mount/point
  -- Options=...
  -- Type=xfs

  -- mount-point.automount
  -- [Automount]
  -- Where=/mount/point
  -- [Install]
  -- WantedBy=multi-user.target

  -- systemctl daemon-reload
  -- systemctl enable --now mount-point.automount

getProxy :: Network -> Proxy
getProxy Res = Proxy "# SOLEIL RES Proxy" "195.221.10.35" "8080"
getProxy Rel = Proxy "# SOLEIL REL Proxy" "195.221.10.6" "8080"

getProxyUrl :: Proxy -> Url
getProxyUrl (Proxy _ ip port) = "http://" <> ip <> ":" <> port

instance ToScript Network where
  toScript n
    = toScript [ script $ FileContent "/etc/environment.d/20-soleil.conf" Nothing
                 [ comment
                 , "http_proxy=" <> url
                 , "https_proxy=" <> url
                 , "no_proxy=localhost"
                 ]
               , script $ FileContent "/etc/apt/apt.conf.d/20proxy" Nothing
                 [ "Acquire::HTTP::Proxy \"" <> url <> "\";"
                 , "Acquire::HTTPS::Proxy \"" <> url <> "\";"
                 ]
               ]
    where
      p@(Proxy comment _ _) = getProxy n
      url = getProxyUrl p

instance ToScript Ntp where
  toScript (Ntp d mr)
    = toScript [ script $ Echo ("Configuring Ntp with this server: " <> server)
               , script install
               , script $ conf d
               , script restart
               ]
    where
      install = InstallPkgs [ "ntpsec" ]

      server = case mr of
                 Nothing  -> ""
                 Just Res -> "server ntp.synchrotron-soleil.fr nts"
                 Just Rel -> "server ntp.exp.synchrotron-soleil.fr nts"

      conf d' = case d' of
                  Upgrade _ t -> conf t
                  Testing -> undefined
                  Stable "bookworm" -> FileContent "/etc/ntpsec/ntp.conf" Nothing
                                      [ "# DO NOT EDIT, MANAGED BY GRADES"
                                      , "#"
                                      , "# /etc/ntpsec/ntp.conf, configuration for ntpd; see ntp.conf(5) for help"
                                      , ""
                                      , "driftfile /var/lib/ntpsec/ntp.drift"
                                      , "leapfile /usr/share/zoneinfo/leap-seconds.list"
                                      , ""
                                      , "# To enable Network Time Security support as a server, obtain a certificate"
                                      , "# (e.g. with Let's Encrypt), configure the paths below, and uncomment:"
                                      , "# nts cert CERT_FILE"
                                      , "# nts key KEY_FILE"
                                      , "# nts enable"
                                      , ""
                                      , "# You must create /var/log/ntpsec (owned by ntpsec:ntpsec) to enable logging."
                                      , "#statsdir /var/log/ntpsec/"
                                      , "#statistics loopstats peerstats clockstats"
                                      , "#filegen loopstats file loopstats type day enable"
                                      , "#filegen peerstats file peerstats type day enable"
                                      , "#filegen clockstats file clockstats type day enable"
                                      , ""
                                      , "# This should be maxclock 7, but the pool entries count towards maxclock."
                                      , "tos maxclock 11"
                                      , ""
                                      , "# Comment this out if you have a refclock and want it to be able to discipline"
                                      , "# the clock by itself (e.g. if the system is not connected to the network)."
                                      , "tos minclock 4 minsane 3"
                                      , ""
                                      , "# Specify one or more NTP servers."
                                      , ""
                                      , "# Public NTP servers supporting Network Time Security:"
                                      , server
                                      , ""
                                      , "# pool.ntp.org maps to about 1000 low-stratum NTP servers.  Your server will"
                                      , "# pick a different set every time it starts up.  Please consider joining the"
                                      , "# pool: <https://www.pool.ntp.org/join.html>"
                                      , "# pool 0.debian.pool.ntp.org iburst"
                                      , "# pool 1.debian.pool.ntp.org iburst"
                                      , "# pool 2.debian.pool.ntp.org iburst"
                                      , "# pool 3.debian.pool.ntp.org iburst"
                                      , ""
                                      , "# Access control configuration; see /usr/share/doc/ntpsec-doc/html/accopt.html"
                                      , "# for details."
                                      , "#"
                                      , "# Note that \"restrict\" applies to both servers and clients, so a configuration"
                                      , "# that might be intended to block requests from certain clients could also end"
                                      , "# up blocking replies from your own upstream servers."
                                      , ""
                                      , "# By default, exchange time with everybody, but don't allow configuration."
                                      , "restrict default kod nomodify nopeer noquery limited"
                                      , ""
                                      , "# Local users may interrogate the ntp server more closely."
                                      , "restrict 127.0.0.1"
                                      , "restrict ::1"
                                      ]
                  Stable "trixie" -> FileContent "/etc/ntpsec/ntp.conf" Nothing
                                    [ "# DO NOT EDIT, MANAGED BY GRADES"
                                    , "#"
                                    , "# /etc/ntpsec/ntp.conf, configuration for ntpd; see ntp.conf(5) for help"
                                    , ""
                                    , "driftfile /var/lib/ntpsec/ntp.drift"
                                    , "leapfile /usr/share/zoneinfo/leap-seconds.list"
                                    , ""
                                    , "# To enable Network Time Security support as a server, obtain a certificate"
                                    , "# (e.g. with Let's Encrypt), configure the paths below, and uncomment:"
                                    , "# nts cert CERT_FILE"
                                    , "# nts key KEY_FILE"
                                    , "# nts enable"
                                    , ""
                                    , "# You must create /var/log/ntpsec (owned by ntpsec:ntpsec) to enable logging."
                                    , "#statsdir /var/log/ntpsec/"
                                    , "#statistics loopstats peerstats clockstats"
                                    , "#filegen loopstats file loopstats type day enable"
                                    , "#filegen peerstats file peerstats type day enable"
                                    , "#filegen clockstats file clockstats type day enable"
                                    , ""
                                    , "# This should be maxclock 7, but the pool entries count towards maxclock."
                                    , "tos maxclock 11"
                                    , ""
                                    , "# Comment this out if you have a refclock and want it to be able to discipline"
                                    , "# the clock by itself (e.g. if the system is not connected to the network)."
                                    , "tos minclock 4 minsane 3"
                                    , ""
                                    , "# Specify one or more NTP servers."
                                    , ""
                                    , "# Public NTP servers supporting Network Time Security:"
                                    , server
                                    , ""
                                    , "# pool.ntp.org maps to about 1000 low-stratum NTP servers.  Your server will"
                                    , "# pick a different set every time it starts up.  Please consider joining the"
                                    , "# pool: <https://www.pool.ntp.org/join.html>"
                                    , "# pool 0.debian.pool.ntp.org iburst"
                                    , "# pool 1.debian.pool.ntp.org iburst"
                                    , "# pool 2.debian.pool.ntp.org iburst"
                                    , "# pool 3.debian.pool.ntp.org iburst"
                                    , ""
                                    , "# Access control configuration; see /usr/share/doc/ntpsec-doc/html/accopt.html"
                                    , "# for details."
                                    , "#"
                                    , "# Note that \"restrict\" applies to both servers and clients, so a configuration"
                                    , "# that might be intended to block requests from certain clients could also end"
                                    , "# up blocking replies from your own upstream servers."
                                    , ""
                                    , "# By default, exchange time with everybody, but don't allow configuration."
                                    , "restrict default kod nomodify noquery limited"
                                    , ""
                                    , "# Local users may interrogate the ntp server more closely."
                                    , "restrict 127.0.0.1"
                                    , "restrict ::1"
                                    ]
                  Stable _ -> error "Not support OS"

      restart = SystemCtl (SDRestart "ntpsec")

instance ToScript Raw where
  toScript (Raw t) = [ t ]

instance ToScript Rm where
  toScript (Rm opts filename)
    = toScript $ Raw ("rm " <> opts <> " " <> filename)

instance ToScript RmCruft where
  toScript RmCruft
    = toScript [ script $ Echo "Removed cruft from previous systems"
               , script $ Rm "-rf" "/usr/local/propellor/"
               , script $ Rm "-f" "/etc/cron.d/propellor"
               , script $ Rm "-f" "/usr/local/bin/propellor_cronjob"
               , script $ Rm "-f" "/usr/local/bin/propellor_cronjob.orig"
               , script $ Rm "-f" "/etc/apt/sources.list.d/sources.list"
               , script $ Rm "-rf" "/srv/chroot/unstable-amd64.shit"
               ]

getBeamlineName :: Beamline -> Text
getBeamlineName Sixs = "sixs"

mkRuche :: Beamline -> Text -> Mount
mkRuche b mp =  Mount wa we "nfs" "rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp"
  where
    wa = case b of
           Sixs -> "ruche-sixs.exp.synchrotron-soleil.fr:/" <> mp
    we = "/nfs/ruche/" <> mp

instance ToScript Ruche where
  toScript (Ruche b)
    = toScript [ script $ InstallPkgs [ "nfs-common" ]
               , script $ mkRuche b (bname <> "-user")
               , script $ mkRuche b (bname <> "-soleil")
               ]
    where
      bname:: Text
      bname = getBeamlineName b


instance ToScript Ssh where
  toScript Ssh
    = toScript [ script install
               , script configure
               , script reload
               ]
    where
      install = InstallPkgs [ "openssh-server" ]

      configure = [ FileContent "/etc/ssh/sshd_config.d/soleil.conf" Nothing
                    [ "PermitRootLogin yes"
                    , "StreamLocalBindUnlink yes"
                    ]
                  ]

      reload = SystemCtl (SDRestart "ssh")

-- dpkg-reconfigure -f noninteractive openssh-server
-- systemctl stop ssh
-- systemctl start ssh

-- mkdir -p /root/.ssh
-- touch /root/.ssh/authorized_keys
-- echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGkFpSsCIGpAJtsH4TWHCatHMkdGMS/PTG2M/7xeWz6Syw/JUrZPc/5bRC9H5+bikrhotZOidC+lafzGFHGmHzpq7+rXrd5Np3uVHH6U+Y0O7mUeU0CVhCpkIr2ggk4Bw7K79/d6fsPXZi2h+JAZ9cBaI6ob5K6e70Ljj3REZRh7LXBVIAd1hmMPEESb5xll1MHvB/7Qn6r6uupcOY/1pC/LH+ZPUaqvwXGrSltFjJoeFEW8H05uYkuZta5vBG/owdLjRt6v7h3tnINsMV4S0uKNQNz6022xAptn1FY1WQ0F1y738hTNoikITty//MB3HW3uQEpw4sXN7tEGqQtHrbMkPfcwb+KMISXYlHPaBt9ik4fWnt55U1IzXr5s/ErT6/ZCG2iPfnffuHnCVMujrUu+KcnHtF7Ux50N1QxR7+EiT6WxRDW3S6Vz0MQ6jTZdy/YryKYZtGnriFb2RwR7u9Y7Df+VYfj4nKrnF3JQF9yipBLcUhpliNvByvoh7eTE8iWuVlp3GkdHotEq4okH88TtUG5DBbddGHoGpxnzi8R4sn+YvFTybyw0whKgMQh0ueJ26j326AgujBDlvL3Hf6Satz/EDmwjStWGSwWQAcy+W+gfNAuRfHpyYHKDGPIJLzMfuf0vx0KLL0C55x7I4cGqOIT22RXLhhf9NFHNDi4Q== cardno:000500003084" > /root/.ssh/authorized_keys

-- # Setup gpg

-- echo "$0: Setup gpg"
-- apt install -y -qq psmisc dirmngr curl scdaemon
-- killall -q ssh-agent || true
-- killall -q gpg-agent || true
-- KEY=0x5632906F4696E015
-- if [[ $(gpg --list-keys | grep -w ${KEY}) ]]; then
--     echo "$0: Key exists: ${KEY}"
-- else
--     curl https://people.debian.org/~picca/pgp_keys.asc | gpg --import
-- fi
-- export GPG_TTY=$(tty)
-- eval $(gpg-agent --daemon --enable-ssh-support)
-- gpg-connect-agent updatestartuptty /bye > /dev/null
-- if [ -S $(gpgconf --list-dirs agent-ssh-socket) ]; then
--     export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
-- else
--     echo "$0: $(gpgconf --list-dirs agent-ssh-socket) doesn't exist. Is gpg-agent running ?"
-- fi

instance ToScript Sudoers where
  toScript (Sudoers us)
    = toScript [ script install
               , script addusers
               , script preserve
               ]
      where
        install = InstallPkgs [ "sudo" ]
        addusers = [[ Raw $ "adduser " <> u <> " sudo"
                    , Raw $ "usermod -aG sudo " <> u ]
                   | u <- us ]
        preserve = FileContent "/etc/sudoers.d/soleil" (Just "440")
                   [ "Defaults:%sudo env_keep += \"http_proxy https_proxy ftp_proxy all_proxy no_proxy\"" ]

instance ToScript SystemD where
  toScript (SystemCtl action)
    = toScript $ case action of
                   SDDaemonReload -> [ script $ Raw "systemctl daemon-reload" ]
                   SDEnable service -> [ script $ Raw $ "systemctl enable " <> service ]
                   SDMask service -> [ script $ Raw $ "systemctl mask " <> service ]
                   SDStart service -> [ script $ Raw $ "systemctl start " <> service ]
                   SDStop service -> [ script $ Raw $ "systemctl stop " <> service ]
                   SDRestart service -> [ script $ SystemCtl (SDStop service)
                                       , script $ SystemCtl (SDStart service)
                                       ]
  toScript (SystemDUnit n content)
    = toScript $ FileContent ("/etc/systemd/system/" <> fromUnitName n) Nothing content

instance ToScript Timezone where
  toScript (Timezone tz) = toScript $ Raw ( "timedatectl set-timezone " <> tz )

instance ToScript Wm where
  toScript Gnome
    = toScript [ script install
               , script configure
               , script reload
               ]
    where
      install = InstallPkgs [ "gnome" ]
      configure = [ FileContent "/etc/gdm3/greeter.dconf-defaults" Nothing
                    [ "# These are the options for the greeter session that can be set"
                    , "# through GSettings. Any GSettings setting that is used by the"
                    , "# greeter session can be set here."
                    , ""
                    , "# Note that you must configure the path used by dconf to store the"
                    , "# configuration, not the GSettings path."
                    , ""
                    , ""
                    , "# Theming options"
                    , "# ==============="
                    , "#  - Change the GTK+ theme"
                    , "[org/gnome/desktop/interface]"
                    , "# gtk-theme='Adwaita'"
                    , "#  - Use another background"
                    , "[org/gnome/desktop/background]"
                    , "picture-uri='file:///usr/share/backgrounds/desktop-background-SOLEIL.png'"
                    , "# picture-options='zoom'"
                    , "#  - Or no background at all"
                    , "[org/gnome/desktop/background]"
                    , "# picture-options='none'"
                    , "# primary-color='#000000'"
                    , ""
                    , "# Login manager options"
                    , "# ====================="
                    , "[org/gnome/login-screen]"
                    , "logo='/usr/share/images/vendor-logos/logo-text-version-128.png'"
                    , ""
                    , "# - Disable user list"
                    , "disable-user-list=true"
                    , "# - Disable restart buttons"
                    , "# disable-restart-buttons=true"
                    , "# - Show a login welcome message"
                    , "banner-message-enable=true"
                    , "banner-message-text='Beware: no backup on this computer'"
                    , ""
                    , "# Automatic suspend"
                    , "# ================="
                    , "[org/gnome/settings-daemon/plugins/power]"
                    , "# - Time inactive in seconds before suspending with AC power"
                    , "#   1200=20 minutes, 0=never"
                    , "sleep-inactive-ac-timeout=0"
                    , "# - What to do after sleep-inactive-ac-timeout"
                    , "#   'blank', 'suspend', 'shutdown', 'hibernate', 'interactive' or 'nothing'"
                    , "sleep-inactive-ac-type='blank'"
                    , "# - As above but when on battery"
                    , "# sleep-inactive-battery-timeout=1200"
                    , "# sleep-inactive-battery-type='suspend'"
                    ]
                  ]

      reload = SystemCtl (SDMask "sleep.target suspend.target hibernate.target hybrid-sleep.target")


getVersion :: Distribution -> Version
getVersion d
  = case d of
      Stable n -> case n of
                   "bookworm" -> "debian-12"
                   _          -> error "Unsupported version"
      Testing  -> "testing"
      Upgrade _ t -> getVersion t

makeExecutable :: FilePath -> IO ()
makeExecutable f
  = do p <- getPermissions f
       setPermissions f (p {executable = True})

writeShell :: FilePath -> Script -> IO ()
writeShell f ts
  = do Data.Text.IO.writeFile f (unlines ts)
       makeExecutable f

buildHost :: Host -> IO ()
buildHost h@(Host (HostName n) _ _ _) = writeShell f (toScript h)
  where
    f = unpack $ "hostsv2/" <> n <> "-config.sh"


-- TODO

-- apt install -o DPkg::Lock::Timeout=180 -y -qq aptitude

-- aptitude install -o DPkg::Lock::Timeout=180 -y -q $WM dconf-cli nfs-common
-- cp $SCRIPT_DIR/desktop-background-SOLEIL.png /usr/share/backgrounds/desktop-background-SOLEIL.png

-- # global Gnome settings

-- # remove auto-suspend/hibernate
-- sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

-- mkdir -p /etc/dconf/profile

-- FILE=/etc/dconf/profile/user
-- echo "$0: Saving gnome/dconf configuration in ${FILE}"
-- dd status=none of=${FILE} << EOF
-- user-db:user
-- system-db:local
-- EOF

-- FILE=/usr/share/X11/xorg.conf.d/50-iglx.conf
-- dd status=none of=${FILE} << EOF
-- Section "ServerFlags"
--     Option "AllowIndirectGLX" "on"
--     Option "IndirectGLX" "on"
-- EndSection
-- EOF

-- dconf update
