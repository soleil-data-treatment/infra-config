#!/bin/bash

# Actions:
# - Install packages via config-soleil
# - /etc/fstab

# configure: Initiate a SOLEIL GRADES computer configuration (minimal stuff)
#            This script mainly calls `config-soleil` and sets FSTAB.
# usage:
#  `configure`      for an automatic proxy setting (based on IP)
#  `configure res`  for a ReS configuration
#  `configure rel`  for a REL configuration
#  `configure none` for a no-proxy configuration

set -e
SCRIPT_DIR=$(cd `dirname $0` && pwd)

if [ "x$1" = "x-h" ]; then
  echo "$0 [rel|res|-h]"
  echo "  Configure a SOLEIL GRADES computer"
  echo "  The optional argument allows to specify which SOLEIL network should be configured"
  exit
fi

if [ "$(id -u)" -ne 0 ] ; then echo "Please run as root" ; exit 1 ; fi

NET=$1

# auto proxy config
if [ "x$NET" = "x" ]; then
  IP=`ip -o route get "8.8.8.8" 2>/dev/null | sed -e 's/^.* src \([^ ]*\) .*$/\1/'`
  case $IP in
      172*)
          NET=res
	  ;;
      195.221.4.*)
          NET=res
	  ;;
      195.221.*)
          NET=rel
	  ;;
  esac
fi

if [ "x$NET" = "x" ]; then
  NET=none
  echo "$0: WARNING: please specify the network (res,rel,none) as argument. Using $NET"
fi


# install the common part

($SCRIPT_DIR/../../config-soleil $NET)

###################
# configure fstab #
###################

FILE=/etc/fstab
echo "Saved fstab configuration in ${FILE}"
dd status=none of=${FILE} << EOF

# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a device; this may
# be used with UUID= as a more robust way to name devices that works even if
# disks are added and removed. See fstab(5).
#
# <file system>             <mount point>  <type>  <options>  <dump>  <pass>
UUID=ced3313e-a56d-470c-b3b3-43e5f04b6724 /              ext4    defaults,noatime 0 1
UUID=942d9cb1-9de9-4b07-9d2a-8664a83f0583 swap           swap    defaults,noatime 0 0

#ruche

ruche-lucia.exp.synchrotron-soleil.fr:/lucia-soleil       /nfs/ruche/lucia-soleil         nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount 0 2
ruche-lucia.exp.synchrotron-soleil.fr:/lucia-users        /nfs/ruche/lucia-users          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount 0 2
ruche-lucia.exp.synchrotron-soleil.fr:/share-temp        /nfs/ruche/share-temp          nfs    rw,hard,acdirmin=1,vers=3,local_lock=flock,tcp,x-systemd.automount 0 2

EOF

mkdir -p /nfs/ruche/lucia-soleil
mkdir -p /nfs/ruche/lucia-users
mkdir -p /nfs/ruche/share-temp

mount -a

########################
# Configure NVIDIA GPU #
########################

apt install nvidia-driver clinfo nvidia-opencl-icd

##################################
# Install and configure software #
##################################

for s in scientific-software
do
    (cd "$(dirname "$0")"/../../software && ./$s.install)
done


