#!/bin/bash

# config-soleil-bootstrap: Initiate a SOLEIL GRADES computer configuration (minimal stuff)
# usage:
#  `config-soleil-bootstrap`      for an automatic proxy setting (based on IP)
#  `config-soleil-bootstrap res`  for a ReS configuration
#  `config-soleil-bootstrap rel`  for a REL configuration
#  `config-soleil-bootstrap none` for a no-proxy configuration
#
# Actions:
# - /etc/environment
# - /etc/apt/sources.list
# - /etc/apt/apt.conf.d/20proxy
# - update, upgrade, install emacs openssh-server

SCRIPT_DIR=$(cd `dirname $0` && pwd)

if [ "x$1" = "x-h" ]; then
  echo "$0 [rel|res|-h]"
  echo "   Initiate a SOLEIL GRADES computer configuration (minimal stuff: proxy, env var, apt, ssh)"
  echo "   The optional argument allows to specify which SOLEIL network should be configured"
  exit
fi

# Execute as root
if [ "$(id -u)" -ne 0 ] ; then echo "$0: Please run as root" ; exit 1 ; fi

# check the network
NET=$1

# auto proxy config
if [ "x$NET" = "x" ]; then
  IP=`ip -o route get "8.8.8.8" 2>/dev/null | sed -e 's/^.* src \([^ ]*\) .*$/\1/'`
  case $IP in
      172*)
          NET=res
	  ;;
      195.221.4.*)
          NET=res
	  ;;
      195.221.*)
          NET=rel
	  ;;
  esac
fi

case $NET in
    *res*)
        PROXY_COMMENT="SOLEIL RES Proxy"
        PROXY_IP=195.221.0.35
    ;;
    *rel*)
        PROXY_COMMENT="SOLEIL REL Proxy"
        PROXY_IP=195.221.10.6
    ;;
    *)
        PROXY_COMMENT="none"
        PROXY_IP=""
        NET=none
    ;;
esac

# no proxy for the localhost

NO_PROXY=localhost

echo "------------------------------------------------------------"
echo "$0: Starting with $NET"

# save the system proxy

if [ "x$NET" = "xnone" ]; then
  echo "$0: Skipping proxy env"
  rm -f /etc/environment
  rm -f /etc/apt/apt.conf.d/20proxy
else
  echo "$0: Setting $PROXY_COMMENT"
  export PROXY_PORT=8080
  export PROXY_URL=http://$PROXY_IP:$PROXY_PORT

  FILE=/etc/environment
  echo "$0: Saving $NET ${PROXY_COMMENT} ${PROXY_URL} in ${FILE}"
  export http_proxy=${PROXY_URL}
  export https_proxy=${PROXY_URL}
  export no_proxy=${NO_PROXY}
  dd status=none of=${FILE} << EOF
  # ${PROXY_COMMENT}
  http_proxy=${PROXY_URL}
  https_proxy=${PROXY_URL}
  no_proxy=${NO_PROXY}
EOF
  # save the apt proxy
  FILE=/etc/apt/apt.conf.d/20proxy
  echo "$0: Saving ${PROXY_COMMENT} ${PROXY_URL} in ${FILE}"
  dd status=none of=${FILE} << EOF
Acquire::HTTP::Proxy "${PROXY_URL}";
Acquire::HTTPS::Proxy "${PROXY_URL}";
EOF
fi

# set the right timezone

TIMEZONE="Europe/Paris"
echo "$0: Setting the ${TIMEZONE} timezone"

timedatectl set-timezone ${TIMEZONE}

# check the Debian version

VERSION=`cat /etc/debian_version`
case $VERSION in
	11.*)
	DEBIAN_CODE="bullseye"
	;;
	12.*)
	DEBIAN_CODE="bookworm"
	;;
	*/sid)
	DEBIAN_CODE="unstable"
esac

echo "removing cruft from previous configuration system"

rm -f /etc/apt/sources.list.d/sources.list
echo "/etc/apt/sources.list.d/sources.list"
rm -rf /srv/chroot/unstable-amd64.shit
echo "/srv/chroot/unstable-amd64.shit/"

echo "Found Debian $DEBIAN_CODE"

# Set apt sources.list
FILE=/etc/apt/sources.list
case $DEBIAN_CODE in
    unstable|testing)
        SOURCES_LIST="
deb http://deb.debian.org/debian $DEBIAN_CODE main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian $DEBIAN_CODE main contrib non-free non-free-firmware
"
        ;;
    stable|bookworm)
        SOURCES_LIST="
deb http://deb.debian.org/debian ${DEBIAN_CODE} main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian ${DEBIAN_CODE} main contrib non-free non-free-firmware
deb http://deb.debian.org/debian ${DEBIAN_CODE}-updates main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-updates main contrib non-free non-free-firmware
deb http://deb.debian.org/debian ${DEBIAN_CODE}-proposed-updates main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-proposed-updates main contrib non-free non-free-firmware
deb http://deb.debian.org/debian ${DEBIAN_CODE}-backports main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-backports main contrib non-free non-free-firmware
deb http://security.debian.org/debian-security ${DEBIAN_CODE}-security main contrib non-free non-free-firmware
deb-src http://security.debian.org/debian-security ${DEBIAN_CODE}-security main contrib non-free non-free-firmware

deb-src http://deb.debian.org/debian testing main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian unstable main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian experimental main contrib non-free non-free-firmware
"
        ;;
    *)
        SOURCES_LIST="
deb http://deb.debian.org/debian ${DEBIAN_CODE} main contrib non-free
deb-src http://deb.debian.org/debian ${DEBIAN_CODE} main contrib non-free
deb http://deb.debian.org/debian ${DEBIAN_CODE}-updates main contrib non-free
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-updates main contrib non-free
deb http://deb.debian.org/debian ${DEBIAN_CODE}-backports main contrib non-free
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-backports main contrib non-free
deb http://deb.debian.org/debian ${DEBIAN_CODE}-backports-sloppy main contrib non-free
deb-src http://deb.debian.org/debian ${DEBIAN_CODE}-backports-sloppy main contrib non-free
deb http://security.debian.org/debian-security ${DEBIAN_CODE}-security main contrib non-free
deb-src http://security.debian.org/debian-security ${DEBIAN_CODE}-security main contrib non-free

deb-src http://deb.debian.org/debian unstable main contrib non-free
"
        ;;
esac
dd status=none of=${FILE} << EOF
${SOURCES_LIST}
EOF

echo "$0: Set Debian ${DEBIAN_CODE} in ${FILE}"


export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true

apt -o DPkg::Lock::Timeout=60 update
apt -o DPkg::Lock::Timeout=60 dist-upgrade -y

# install emacs

echo "$0: Installing emacs and elpa-magit"
apt -o DPkg::Lock::Timeout=60 install -y emacs elpa-magit

# ssh

apt install -y -qq openssh-server

FILE=/etc/ssh/sshd_config.d/soleil.conf
echo "$0: save sshd config in $FILE"
dd status=none of=${FILE} << EOF
PermitRootLogin yes
StreamLocalBindUnlink yes
EOF

dpkg-reconfigure -f noninteractive openssh-server
systemctl stop ssh
systemctl start ssh

mkdir -p /root/.ssh
touch /root/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGkFpSsCIGpAJtsH4TWHCatHMkdGMS/PTG2M/7xeWz6Syw/JUrZPc/5bRC9H5+bikrhotZOidC+lafzGFHGmHzpq7+rXrd5Np3uVHH6U+Y0O7mUeU0CVhCpkIr2ggk4Bw7K79/d6fsPXZi2h+JAZ9cBaI6ob5K6e70Ljj3REZRh7LXBVIAd1hmMPEESb5xll1MHvB/7Qn6r6uupcOY/1pC/LH+ZPUaqvwXGrSltFjJoeFEW8H05uYkuZta5vBG/owdLjRt6v7h3tnINsMV4S0uKNQNz6022xAptn1FY1WQ0F1y738hTNoikITty//MB3HW3uQEpw4sXN7tEGqQtHrbMkPfcwb+KMISXYlHPaBt9ik4fWnt55U1IzXr5s/ErT6/ZCG2iPfnffuHnCVMujrUu+KcnHtF7Ux50N1QxR7+EiT6WxRDW3S6Vz0MQ6jTZdy/YryKYZtGnriFb2RwR7u9Y7Df+VYfj4nKrnF3JQF9yipBLcUhpliNvByvoh7eTE8iWuVlp3GkdHotEq4okH88TtUG5DBbddGHoGpxnzi8R4sn+YvFTybyw0whKgMQh0ueJ26j326AgujBDlvL3Hf6Satz/EDmwjStWGSwWQAcy+W+gfNAuRfHpyYHKDGPIJLzMfuf0vx0KLL0C55x7I4cGqOIT22RXLhhf9NFHNDi4Q== cardno:000500003084" > /root/.ssh/authorized_keys

# Setup gpg

echo "$0: Setup gpg"
apt install -y -qq psmisc dirmngr curl scdaemon
killall -q ssh-agent || true
killall -q gpg-agent || true
KEY=0x5632906F4696E015
if [[ $(gpg --list-keys | grep -w ${KEY}) ]]; then
    echo "$0: Key exists: ${KEY}"
else
    curl https://people.debian.org/~picca/pgp_keys.asc | gpg --import
fi
export GPG_TTY=$(tty)
eval $(gpg-agent --daemon --enable-ssh-support)
gpg-connect-agent updatestartuptty /bye > /dev/null
if [ -S $(gpgconf --list-dirs agent-ssh-socket) ]; then
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
else
    echo "$0: $(gpgconf --list-dirs agent-ssh-socket) doesn't exist. Is gpg-agent running ?"
fi

case $1 in
    publish)
	echo "$0: Publishing to people.debian.org"
	scp $SCRIPT_DIR/bootstrap-soleil.sh picca@people.debian.org:~/public_html/
	exit
	;;
esac

# run the specific setup for the computer
HOSTNAME="$(hostname --fqdn)"

if [[ -d "$SCRIPT_DIR/hosts/$HOSTNAME" && -f $SCRIPT_DIR/hosts/$HOSTNAME/configure ]]
then
    ($SCRIPT_DIR/hosts/$HOSTNAME/configure $NET)
else
    echo "$0: Setup a new host $HOSTNAME"

    cp -r $SCRIPT_DIR/hosts/tmpl/ $SCRIPT_DIR/hosts/$HOSTNAME

    # keep the current fstab
    sed -i -e '/FSTAB_CONTENT/r /etc/fstab' -e 's/FSTAB_CONTENT//' $SCRIPT_DIR/hosts/$HOSTNAME/configure

    chown -R $SUDO_UID $SCRIPT_DIR/hosts/$HOSTNAME

    echo "$0: Please customize the configuration at $SCRIPT_DIR/hosts/$HOSTNAME/configure"
fi

echo "$0: Done with $NET"
echo "------------------------------------------------------------"
