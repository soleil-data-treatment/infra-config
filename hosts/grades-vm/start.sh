#!/bin/bash
#
# Usage:
#   start.sh {-h|-s|-g GPU} VM|ISO
#   -e QEMU_EXE run given executable for QEMU e.g. 'qemu-system-x86_64 -enable-kvm -cpu host'
#   -g GPU_PCI  assign GPU at PCI address e.g. '-g 0000:47:00.0'
#   -h          show help
#   -m MEM_MB   memory in MB
#   -n MEM_MB   use given nb of CPU
#   -s          use spicy instead of qemu base viewer (spice-client-gtk)

# defaults
VM=
USE_SPICE=
USE_GPU=
# QEMU options: edit to your needs, if necessary
USE_MEM=4096
USE_CPU=4
USE_KVM="-enable-kvm -cpu host"
USE_VGA="std"

# QEMU executables: you may set QEMU=qemu-kvm as well or leav empty for auto
QEMU=
QEMU_IMG=qemu-img

# auto-determine QEM executable if not set
ARCH=`uname -m`
if [ "x$QEMU" = "x" ]; then
    QEMU="qemu-system-$ARCH"
fi

while getopts hKsg:m:n:c:e:a: flag
do
    case "${flag}" in
        h) # display help
        echo "$0 {-h|-e QEMU_EXE|-s|-g GPU_PCI|-m MEM_MB|-n USE_CPU|-s} VM|ISO ..."
        echo "Start qemu with given virtual machine or ISO and options."
        echo "   -e QEMU_EXE run given executable for QEMU (default '$QEMU')"
        echo "   -g GPU_PCI  assign GPU at PCI address e.g. '-g 0000:47:00.0'"
        echo "   -h          show help"
        echo "   -K          do NOT use KVM     (emulation, default with KVM)"
        echo "   -m MEM_MB   memory in MB                  (default $USE_MEM)"
        echo "   -n CPU_MB   use given nb of CPU           (default $USE_CPU)"
        echo "   -s          use spicy instead of qemu base viewer (spice-client-gtk)"
        echo "Additional arguments are passed to QEMU."
        exit
        ;;
        a|e)  # executable/arch
        QEMU=${OPTARG}
        ;;
        g)  # use GPU at given PCI
        GPU=${OPTARG}
        USE_GPU="-device vfio-pci,host=$GPU,multifunction=on"
        ;;
        K)  # no KVM
        USE_KVM=
        ;;
        m)  # use memory in MB
        USE_MEM=${OPTARG}
        ;;
        n|c)  # use #CPU
        USE_CPU=${OPTARG}
        ;;
        s)  # use spice/spicy
        USE_SPICE="-spice port=5900,addr=127.0.0.1,disable-ticketing=on"
        ;;
    esac
done
shift $((OPTIND-1)) 

if ! command -v $QEMU; then
  echo "ERROR: unable to set executable: command not found: $QEMU Use option '-e QEMU_EXE'."
  exit 1
fi

# remaining arguments -> VM
VM=$@

if [ "x$VM" = "x" ]; then
  echo "$0 {-h|-e QEMU_EXE|-s|-g GPU_PCI|-m MEM_MB|-n USE_CPU|-s} VM|ISO ..."
  exit
fi

# default arguments for QEMU
QEMU="$QEMU -m $USE_MEM -smp $USE_CPU -vga $USE_VGA -device ich9-ahci,id=ahci -netdev user,id=mynet0 -device virtio-net,netdev=mynet0 -device virtio-balloon $USE_GPU $USE_KVM"

EXT="${VM##*.}"
if [ "x$EXT" = "xiso" ]; then
  ISO=$VM
  filename="${VM%.*}"
  VM="${filename}.qcow2"
  echo "Creating disk $VM from $ISO"
  $QEMU_IMG create -f qcow2 $VM 40G || FAILED=1
  if [ "x$FAILED" = "x1" ]; then  # failed ? use qcow2 in /tmp/
    VM=$(basename $VM)
    VM=/tmp/$VM
    $QEMU_IMG create -f qcow2 $VM 40G
  fi
  QEMU="$QEMU -boot d -cdrom $ISO"
fi

echo " "
echo "Starting $VM"
if [ "x$USE_SPICE" = "x" ]; then
  echo "$QEMU -hda $VM"
  $QEMU -hda $VM
else
  echo "$QEMU -hda $VM $USE_SPICE"
  $QEMU -hda $VM $USE_SPICE &
  QEMU_PID=$!
  sleep 2
  spicy -p 5900
  echo "Kill process $QEMU_PID with $QEMU $VM"
  kill $QEMU_PID
fi

echo Done $@

