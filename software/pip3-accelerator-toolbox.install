#!/bin/sh

set -e

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
  export http_proxy
  export https_proxy
fi

export DEBIAN_FRONTEND=noninteractive 
export DEBCONF_NONINTERACTIVE_SEEN=true

echo "$0: start Installing AT/Badger (proxy=$http_proxy)"
apt install -y -qq aptitude python3-venv jupyter

# create a local venv (to be shared)
NAME=pyat_badger
TARGET=/opt/venvs/$NAME
python3 -m venv $TARGET # --system-site-packages
. $TARGET/bin/activate

pip3 install --proxy=$http_proxy accelerator-toolbox badger-opt jupyter 

# create launcher
FILE=/usr/local/bin/$NAME
dd status=none of=${FILE} << EOF
#!/bin/sh
source $TARGET/bin/activate
python3 -m ipykernel install --user --name=$NAME
echo "You may now launch: jupyter notebook"
bash
EOF
chmod a+x $FILE

FILE=/usr/share/applications/$NAME.desktop
dd status=none of=${FILE} << EOF
[Desktop Entry]
Name=Accelerator Toolbox and Badger Notebook
Comment=Accelerator Toolbox and Badger Jupyter Notebook
Exec=bash -c "source $TARGET/bin/activate; python3 -m ipykernel install --user --name=$NAME; jupyter notebook"
Terminal=true
Type=Application
Categories=Development;Graphics;Education;
StartupNotify=true
EOF

# create launcher
FILE=/usr/share/applications/$NAME-terminal.desktop
dd status=none of=${FILE} << EOF
[Desktop Entry]
Name=Accelerator Toolbox and Badger (Terminal)
Comment=Accelerator Toolbox and Badger Terminal
Exec=bash -c "source $TARGET/bin/activate; exec bash"
Terminal=true
Type=Application
Categories=Development;Graphics;Education;
StartupNotify=true
EOF

# these directories should exist, and be stored at the user level.
#FILE=/etc/profile.d/badger.sh
#echo "export BADGER_ARCHIVE_ROOT=/opt/venv/$NAME/archive" >> ${FILE}
#echo "export BADGER_LOGBOOK_ROOT=/opt/venv/$NAME/logbook" >> ${FILE}
#echo "export BADGER_DB_ROOT=/opt/venv/$NAME/database" >> ${FILE}
#echo "export BADGER_PLUGIN_ROOT=/opt/venv/$NAME/plugin" >> ${FILE}

find $TARGET -type d -exec chmod 777 {} \;
find $TARGET -type f -exec chmod a+r {} \;

echo "$0: Done"
