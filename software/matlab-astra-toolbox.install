#!/bin/sh

set -e

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
  export http_proxy
  export https_proxy
fi

echo "$0: start Installing ASTRA toolbox for Matlab"

apt install -y -qq unzip automake autogen libtool nvidia-cuda-toolkit

# there is an issue with Debian 12+/Ubuntu 21+ when check if nvcc works
# In /usr/include/c++/11/type_traits
# need to comment lines ~79-80
#   template<typename _Tp
#   constexpr _Tp integral_constant
if [ -e /usr/include/c++/11/type_traits ]; then
  FILE=/usr/include/c++/11/type_traits
  sed -i 's|template<typename _Tp|//template<typename _Tp|' $FILE
  sed -i 's|constexpr _Tp integra|//constexpr _Tp integra|' $FILE
fi

# configure
mkdir -p /opt/matlab
cd /tmp
FILE=astra-toolbox-2.2.0
MATLAB=2024a

curl -k -O https://data-analysis.synchrotron-soleil.fr/tmp/matlab/toolboxes/${FILE}.zip
unzip ${FILE}.zip
cd $FILE/build/linux
./autogen.sh
./configure --with-cuda=/usr/lib/cuda \
            --with-matlab=/opt/MATLAB/$MATLAB \
            --prefix=/opt/matlab/astra \
            --with-install-type=module
make
make install

cd /tmp
rm -rf astra-toolbox-*

MATLABROOT="$@"
if [ "x$MATLABROOT" = "x" ]; then
  MATLABROOT="/opt/MATLAB/R*"
fi

for m in $MATLABROOT
do
  if [ -f "$m/toolbox/local/startup_soleil.m" ]; then
    echo "disp('Astra Toolbox')" >> $m/toolbox/local/startup_soleil.m
  fi
done

echo "$0: Done"
