# Makefile for the configuration of SOLEIL data treatment computers and VMs
#
# make all
#   configure a local computer, and uses/builds its configuration
# make repo
#   builds our local debian packages (from sandbox)
# make vm
#   builds our current stable VM in REL network
# make vm VM_FLAVOUR=rel,vm-soleil
#   builds a specific VM configuration flavours/hosts/software
#   you may as well specify the target file VM_NAME=/path/to/target.qcow2
#
# required: libguestfs-tools (for VMs)

EDITOR?=emacs -nw
VM_NAME?=$(shell echo /tmp/debian-`date +%Y-%m-%d`.qcow2)
VM_FLAVOUR?="rel,vm-soleil"

all: bootstrap

bootstrap: config-soleil-bootstrap
	./config-soleil-bootstrap

clean:
	make -C sandbox -f Makefile clean

edit:
	$(EDITOR) hosts/$(shell hostname --fqdn)/configure

# local Debian repository handling
repo:
	make -C sandbox -f Makefile repo

bpo:
	make -C sandbox -f Makefile bpo

# VM build
vm:
	@echo "Starting hosts/grades-vm/config-vm -z -o $(VM_NAME) -s $(VM_FLAVOUR)"
	hosts/grades-vm/config-vm -o $(VM_NAME) -z -s $(VM_FLAVOUR)
	@echo "Generated VM $(VM_FLAVOUR) as $(VM_NAME)"

list:
	@grep '^[^#[:space:]].*:' Makefile

help: list


.PHONY: bootstrap clean edit list repo vm
