#!/usr/bin/perl

#  build-repo.pl: build a local repository with Debian backport packages
#  Usage: ./build-repo.pl [--quiet][--continue][--target=DIR]
#    --quiet                          reduce build output.
#    --continue                       do not stop when a package can not be built.
#    --target=DIR                     target where to work (default is current DIR).
#    --distribution=DIST              Debian distribution to build for 9default bookworm).
#    --packages=FILE                  file with a list of packages
#    --packages="suite: pg1 pkg2..."  list of packages, where suite is e.g. 'testing' or 'unstable'

#  The 'packages' file is a free text with lines: (# at start indicates comments)
#
#  testing      PACKAGE
#  unstable     PACKAGE
#  experimental PACKAGE
#  git          PACKAGE https://git-repo-url [subpath]
#  local        PACKAGE GITDIRECTORY
#
#  Empty lines and comments (line starting with #) are allowed.

# Installation and usage:
#   apt install devscripts git libsbuild-perl
#   apt install libgit-repository-perl libapt-pkg-perl libdpkg-perl libdata-dumper-simple-perl libenum-perl libfile-copy-recursive-perl
#   tune the initial variables below (e.g. $distribution)
#   activate Debian source repository
#   customize the packages file
#   run this script

# TODO backport check if the backport is already available in the official repository and skip build
# TODO backport check if the backport is already in the local repository and skip
# TODO git check if the expected version is already in the local repository and skip
# TODO add a force parameter on the line in order to bypass the version check

# use v5.36;
use strict;
use warnings;

no warnings 'experimental';

use AptPkg::Cache;
use AptPkg::Source;
use Cwd;
use Dpkg::Changelog::Parse ();
use Dpkg::Control::Info ();
use Dpkg::Version ();
use File::Copy::Recursive qw(dircopy);
use File::Path qw( rmtree );
use File::stat;
use File::Temp      qw/ tempfile /;
use Git::Repository;
use feature "switch";
use Data::Dumper ();
use Sbuild::Sysconfig ();
use Getopt::Long;
use Time::Local;

use enum qw ( :BACKPORTED_ NONE OFFICIAL LOCAL LOCAL_MISMATCH );

# global variables

my $fn             = 'packages';
my $source         = AptPkg::Source->new();
my $repo           = 'repository';
my $work           = 'workspace';
my $distribution   = 'bookworm-backports';
my $ncore          = 64;
my $quiet          = 0;
my %repo_hash      = ();
my $continue       = 0;
my $help           = 0;
my $cwd            = getcwd();
my @orig_ARGV      = @ARGV;

# ------------------------------------------------------------------------------
# handle options
# TODO backport check if the backport is already available in the official repository and skip build
# TODO backport check if the backport is already in the local repository and skip
# TODO git check if the expected version is already in the local repository and skip
# TODO add a force parameter on the line in order to bypass the version check

# set the environment

$ENV{'DEBIAN_FRONTEND'}             = "noninteractive";
$ENV{'DEBCONF_NONINTERACTIVE_SEEN'} = "true";
$ENV{'DEBEMAIL'}                    = 'picca@debian.org';
$ENV{'DPKG_GENSYMBOLS_CHECK_LEVEL'} = '-c0';
$ENV{'DEB_BUILD_OPTIONS'}           = 'nocheck';
$ENV{'DEB_BUILD_PROFILES'}          = 'nocheck';

# get options

GetOptions("quiet"    => \$quiet,
           "continue" => \$continue,
           "help"     => \$help,
           "target=s" => \$cwd,
           "distribution=s" => \$distribution,
           "packages=s" => \$fn);

if ($help) {
  print STDERR "$0: build a local repository with Debian backport packages\n";
  print STDERR "Usage: $0 [--quiet][--continue][--target=DIR]\n";
  print STDERR "  --quiet             reduce build output.\n";
  print STDERR "  --continue          do not stop when a package can not be built.\n";
  print STDERR "  --target=DIR        target where to work (default is current DIR).\n";
  print STDERR "  --distribution=DIST Debian distribution to build for default $distribution).\n";
  print STDERR "  --packages=FILE     file with a list of packages.\n";
  print STDERR "  --packages=\"suite: pg1 pkg2...\"  list of packages, where suite is e.g. 'testing' or 'unstable'\n";
  print STDERR "\n";
  print STDERR "The 'packages' file is a free text with lines: (# at start indicates comments)\n\n";
  print STDERR "# A list of packages to build\n";
  print STDERR "testing      PACKAGE\n";
  print STDERR "unstable     PACKAGE\n";
  print STDERR "experimental PACKAGE\n";
  print STDERR "git          PACKAGE https://git-repo-url [subpath]\n";
  print STDERR "local        PACKAGE GITDIRECTORY\n\n";
  print STDERR "Empty lines and comments (line starting with #) are allowed.\n";
  print STDERR "\n";
  exit;
}

# prepare the repository

my $repository  = "$cwd/$repo";
my $workspace   = "$cwd/$work";

# print in debug mode
sub qprint {
    my $content = shift;
    print($content) if !($quiet);
}

# execute system command, handling debug mode
sub qsystem {
    my $cmd = shift;
    my $status;

    if ($quiet){
        $cmd .= ' > /dev/null 2>&1';
    }
    qprint("s: $cmd\n");
    $status = system($cmd);
    qprint("s: returned status $status\n");
    return $status;
}

# get version, or a new one
sub get_maybe_version {
    my $mv;
    my $mversion = shift;

    if (not defined $mversion) {
        $mv = $mversion;
    } else {
        $mv = Dpkg::Version->new($mversion);
    }

    return $mv;
}

# get version from package info (madison) and create version when not found
sub get_maybe_version_from_madison {
    my $package = shift;
    my $suite = shift;

    my (undef, $mversion, undef, undef) = split(/\s*\|\s*/, `rmadison --architecture=source --suite $suite $package | tail -n 1`);
    my $mv = get_maybe_version($mversion);
    if (not defined $mv) {
        qprint("v: undef (madison $suite)\n");
    } else {
        qprint("v: '$mv' (madison $suite)\n");
    }

    return $mv;
}

# get version from the changelog
sub get_version_from_changelog() {
    my %options = ( verbose => 1 );
    my $version = Dpkg::Changelog::Parse->changelog_parse( \%options )->{'Version'};
    my $v = Dpkg::Version->new($version);
    qprint("v: '$v' (changelog)\n");
    return $v
}

# get version from the local repo (when already there/previously built)
sub get_maybe_version_from_local_repository {
    my $package = shift;
    my $mversion = $repo_hash{$package};
    my $mv = get_maybe_version($mversion);
    if (not defined $mv) {
        qprint("v: undef (local repository)\n");
    } else {
        qprint("v: '$mv' (local repository)\n");
    }
    return $mv;
}

# get package name and version from Sources file
sub parse_sources() {
    my %sources = ();
    my $p;

    open( SOURCES, "$repository/Sources" ) or return %sources;

    foreach my $line (<SOURCES>) {
        given ($line) {
            when (/^Package:.*/) {
                $line =~ /^Package:\s*(.*)\s*\n/;
                $p = $1;
            }
            when (/^Version:.*/) {
                $line =~ /^Version:\s*(.*)\s*\n/;
                $sources{$p} = Dpkg::Version->new($1);
            }
        }
    }

    close SOURCES;
    return %sources;
}

# clear workspace directory
sub clean_package_workspace {
    my $dir = shift;

    if ( -d $dir ) {
        rmtree $dir;
    }
    mkdir $dir;
}

# update the repository information from the source repo
sub update_repository() {
    # update the repository
    if ($quiet){
        system("cd $repository && apt-ftparchive -q packages . > Packages 2> /dev/null");
        system("cd $repository && apt-ftparchive -q sources . > Sources 2> /dev/null");
        system("cd $repository && apt-ftparchive -q release . > Release 2> /dev/null");
    } else {
        system("cd $repository && apt-ftparchive packages . > Packages");
        system("cd $repository && apt-ftparchive sources . > Sources");
        system("cd $repository && apt-ftparchive release . > Release");
    }
    %repo_hash = parse_sources();
    return 1;
}

# build the package from sources. Return (status, message)
sub sbuild() {
    my @args = (
        'sbuild',              '-s',
        '--force-orig-source', "-j$ncore",
        "--build-dir=$repository",
        '--no-clean-source',
        '--no-run-autopkgtest',
        "--extra-package=$repository",
        "--extra-repository=deb http://deb.debian.org/debian $distribution main contrib non-free non-free-firmware",
        "--extra-repository=deb http://deb.debian.org/debian experimental main contrib non-free non-free-firmware",
        "--build-dep-resolver=aptitude"
    );
    if ($quiet) {
        @args = ( @args, '-q', '--dpkg-source-opt=-q' );
    }

    qprint("d: @args\n");

    # build all packages into $repository
    my $status = system(@args);
    my $message= "";

    if ( $status == 0 ) {
        # move manually the file until we migrate to bookworm
        if (Dpkg::Version->new($Sbuild::Sysconfig::version) < Dpkg::Version->new('0.82.0')){
            qsystem("cp ../*bpo*.deb ../*bpo*.buildinfo ../*bpo*.dsc ../*bpo*.changes ../*bpo*.build ../*tar.* $repository");
        }

        # update the repository
        update_repository();
        $message=" 🟢 (built)";
    }
    else {
        $message=" 🔴 (failed build)";
    }
    return ($status,$message);
}

# check status if already backported
sub already_backported {
    my $mbpo = shift;
    my $vsuite = shift;
    my $mrepo = shift;
    my $res;

    my $bpo_str = "$vsuite~bpo12+1";

    if (not defined $mbpo){
        if (not defined $mrepo){
            $res = BACKPORTED_NONE;
        } else {
            if ($mrepo->as_string() eq $bpo_str){
                $res = BACKPORTED_LOCAL;
            }else{
                $res = BACKPORTED_NONE;
            }
        }
    } else {
        my $suite_str = $vsuite->as_string();

        if ($mbpo->as_string() =~ m/\Q$suite_str\E/){
            $res = BACKPORTED_OFFICIAL;
        } else {
            if (not defined $mrepo){
                $res = BACKPORTED_NONE;
            } else {
                if ($mrepo->as_string() eq $bpo_str){
                    $res = BACKPORTED_LOCAL;
                }else{
                    $res = BACKPORTED_LOCAL_MISMATCH;
                }
            }
        }
    }

    qprint("d: already backported $res\n");
    return $res;
}

# ----------------------------------------------------------------------------

# build a backported package
sub build_backport {
    my $package = shift;
    my $suite   = shift;
    my $status;
    my $message = "";

    print("$package:");

    # getting the suite version from madison
    my $maybe_version_suite       = get_maybe_version_from_madison($package, $suite);
    my $maybe_version_backports   = get_maybe_version_from_madison($package, "stable-backports");
    my $maybe_version_local_repo  = get_maybe_version_from_local_repository($package);

    if (not defined $maybe_version_suite){
        $message=" 🔴 (not in $suite -> check why)";
        $status=1;
    } else {
        my $version_suite = $maybe_version_suite;
        my $backported = already_backported($maybe_version_backports, $version_suite, $maybe_version_local_repo);

        given($backported){
            when(BACKPORTED_NONE){
                # extract the sources
                my $dir = "$workspace/$package";
                clean_package_workspace($dir);
                chdir $dir || return (1," 🔴 Could not access directory $dir");
                $status = qsystem("apt-get source --only-source $package=$version_suite");
                my $version_src = $version_suite->version();
                chdir "$package-$version_src" || return (1," 🔴 Could not get source $package-$version_src: $status");

                # modify the changelog
                qsystem("dch --local=~bpo12+ 'Rebuild for $distribution'");
                qsystem("dch --empty -r --force-distribution -D $distribution ''");
                my $version_changelog = get_version_from_changelog();

                # need a rebuild if the new version is not already available
                print(" backporting from $suite(=$version_suite) to $distribution(=$version_changelog)");
                ($status,$message)=sbuild();
            }
            when(BACKPORTED_OFFICIAL){
                $message=" 🟢 (already uploaded into Debian)";
                $status=0;
            }
            when(BACKPORTED_LOCAL) {
                if ($suite =~ /testing/){
                    $message=" 🟠 (ready to upload into Debian ?)";
                }else{
                    $message=" 🟣 (why not yet in 'testing')";
                }
                $status=0;
            }
            when(BACKPORTED_LOCAL_MISMATCH){
                $message=" 🔴 (please remove the local version of $package(=$maybe_version_local_repo) from $repository and run this script again.)";
                $status=1;
            }
        }
    }
    print("$message\n");
    return ($status, $message);
}

# build a package from git
sub build_git {
    my $package = shift;
    my $url = shift;
    my $subpath = shift;
    my $status;
    my $message="";

    print("$package:");

    # extract the sources
    my $dir = "$workspace/$package";
    clean_package_workspace($dir);
    if ($quiet) {
        Git::Repository->run( clone => $url, $dir, '-q' );
    }
    else {
        Git::Repository->run( clone => $url, $dir );
    }
    my $r = Git::Repository->new( work_tree => $dir );

    chdir "$dir/$subpath" || return (1, " 🔴 Could not access $dir/$subpath");

    my $version_git = get_version_from_changelog();

    qsystem( 'git deborig HEAD' );

    # modify the changelog
    qsystem( "dch --local=~bpo12+ 'Rebuild for $distribution'" );
    qsystem( "dch --empty -l soleil ''" );
    qsystem( "dch --empty -r --force-distribution -D $distribution''" );
    my $version_changelog = get_version_from_changelog();

    # get the new package version
    my $maybe_version_local_repo = get_maybe_version_from_local_repository($package);
    if (not defined $maybe_version_local_repo or $version_changelog > $maybe_version_local_repo){
        print(" building from git $url under $subpath");
        ($status,$message) = sbuild();
    }
    else {
        $message=" 🟠";
        $status = 0;
    }
    print("$message\n");
    return ($status,$message);
}

# build a package from git
sub build_local {
    my $package = shift;
    my $src = shift;
    my $status;
    my $message="";

    print("$package:");

    # extract the sources
    my $dir = "$workspace/$package";
    clean_package_workspace($dir);
    if (not $quiet) {
        print(" copy $src -> $dir\n");
    }
    if (-d "$dir") {
      qsystem( "rm -rf $dir");
    }
    qsystem( "cp -r $src $dir");

    chdir "$dir" || return (1, " 🔴 Could not access $dir");

    # we assume the local directory is a git clone/pull
    qsystem( 'git deborig HEAD' );

    # modify the changelog
    qsystem( "dch --local=~bpo12+ 'Rebuild for $distribution'" );
    qsystem( "dch --empty -l soleil ''" );
    qsystem( "dch --empty -r --force-distribution -D $distribution''" );
    my $version_changelog = get_version_from_changelog();

    # get the new package version
    my $maybe_version_local_repo = get_maybe_version_from_local_repository($package);
    if (not defined $maybe_version_local_repo or $version_changelog > $maybe_version_local_repo){
        print(" building from $src\n");
        ($status,$message) = sbuild();
    }
    else {
        $message=" 🟠";
        $status = 0;
    }
    print("$message\n");
    return ($status,$message);
}

# convert string of packages into a temporary file
sub apt_list_to_file {
  my $string=shift;    # e.g. "$suite: pkg1 pkg2 ..."

  # extract 'suite' from package list
  my ($suite, $pkgs) = split(':\s*', $string);
  my @pkg=split /\s+/,$pkgs;

  # create temporary file
  my ($token_handle, $fn) = tempfile(UNLINK => 1);

  # write list of packages

  foreach my $pkg (@pkg) {
    print $token_handle "$suite: $pkg\n";
  }
  close($token_handle);

  return $fn;
}

# ------------------------------------------------------------------------------

my $begin = localtime();
if (! -d "$cwd") {
  mkdir "$cwd" || die " 🔴 Can not create $cwd\n";
}
mkdir "$repository" || die " 🔴 Can not create $repository\n";
mkdir "$workspace"  || die " 🔴 Can not create $workspace\n";

# regenerate the Debian repo Packages in case it was modified from outside...
update_repository() || print(" 🔴 Failed update_repository: $!\n");

# test if the packages are given as a list of names (not a file)
if(! -e "$fn") {
  $fn =  apt_list_to_file($fn);
}

# open the packages file and do the job.
open( PACKAGES, $fn ) or die(" 🔴 Could not open  file '$fn'\n");

open( FH, '>', "$workspace/packages_done");
print FH "# packages to be built in the deb repo ($0)\n";
print FH "# usage: ./build-repo.pl [--continue][--quiet][--packages=FILE][--help]\n";
print FH "#\n# packages that fail are prepended with '#'\n";

my $title="$0: Debian repository build: $distribution ($begin)\n";
my $count=0;
my $count_failed=0;
open(FHTML, '>', "$workspace/packages.html");
print FHTML "<!DOCTYPE html>\n";
print FHTML "<html><head><title>$title</title></head>\n";
print FHTML "<body><h1>$title</h1>\n";
print FHTML "<p>Build a local repository from git and source deb for backport. Remember to perform an <b>'apt update'</b> in case some source code is not available.</p>\n<p>Command: $0 ";
print FHTML "$_" foreach @orig_ARGV;
print FHTML "</p>\n<table border=1>\n";
print FHTML "<tr> <th>#</th> <th>type</th> <th>Package log</th> <th>Source code</th> <th>Status</th> </tr>\n";

foreach my $line (<PACKAGES>) {
    my $status;
    my $message="";
    my $package="$0";
    my $url;
    my $subpath;
    my $type="";

    given ($line) {
        when (/^#.*/) { next; }
        when (/^testing.*/) {
            ( $type, $package ) = split( " ", $line );
            ($status,$message)=build_backport( $package, 'testing' );
        }
        when (/^unstable.*/) {
            ( $type, $package ) = split( " ", $line );
            ($status,$message)=build_backport( $package, 'unstable' );
        }
        when (/^experimental.*/) {
            ( $type, $package ) = split( " ", $line );
            ($status,$message)=build_backport( $package, 'experimental' );
        }
        when (/^git.*/) {
            ( $type, $package, $url, $subpath ) = split( " ", $line );
            ($status,$message)=build_git( $package, $url, $subpath );
        }
        when (/^local.*/) {
            ( $type, $package, $subpath ) = split( " ", $line );
            ($status,$message)=build_local( $package, $subpath );
        }
    }
    # generate a report file (comment packages that can not be compiled)
    if ($status) {
      $line = "# $line"; # failed -> commented
    }
    print FH $line;

    # generate HTML report line
    if ($package !~ "$0") {
      $count = $count+1;
      # get log file and work directory
      my @log_files  = sort {stat($a)->mtime <=> stat($b)->mtime} glob("$repository/$package*.build");
      my $log_last   = $log_files[-1];
      my @work_files = glob("$workspace/$package/*");
      my $work_file_nb = @work_files;
      print FHTML "<tr> <td>$count</td> <td>$type</td> ";

      if ($log_last && -f $log_last) {
        print FHTML "<td><a href='$log_last' target=_blank>$package</a>";
      } else  {
        print FHTML "<td>$package";
      }
      print FHTML " [<a href='https://tracker.debian.org/pkg/$package' target=_blank>tracker</a>]</td>";

      if ($work_file_nb && -d "$workspace/$package") {
        print FHTML "<td><a href='$workspace/$package'>source code</a> </td>";
      } else {
        print FHTML "<td> </td>";
      }
      if ($status) {
        print FHTML "<td><b>$message</b></td> </tr>\n";
        $count_failed = $count_failed +1;
      } else {
        print FHTML "<td>$message</td> </tr>\n";
      }
    }

    if ($status && ! $continue) {last;}
}
my $end = localtime();

print FHTML "</table><hr><p>Generated $end</p>\n";
print FHTML "<p>The actual processed package file is <a href='$workspace/packages_done'>$workspace/packages_done</a></p>\n";
print FHTML "<p>$count_failed failed packages out of $count total.</p>\n";
print FHTML "</body></html>\n";

close (FHTML);
close (FH);
close PACKAGES;

print($begin, " -> ", $end, "\n");
print("The resulting packages are in         $repository\n");
print("The actual processed package file is: $workspace/packages_done\n");
print("Report has been generated in:         file://$workspace/packages.html\n");
print("Failed packages:                      $count_failed out of $count total.\n");

# end of build-repo
