#!/bin/bash

# Actions:
# - Install packages via config-soleil
# - /etc/fstab

# configure: Initiate a SOLEIL GRADES computer configuration (minimal stuff)
#            This script mainly calls `config-soleil` and sets FSTAB.
# usage:
#  `configure`      for an automatic proxy setting (based on IP)
#  `configure res`  for a ReS configuration
#  `configure rel`  for a REL configuration
#  `configure none` for a no-proxy configuration

set -e
SCRIPT_DIR=$(cd `dirname $0` && pwd)

if [ "x$1" = "x-h" ]; then
  echo "$0 [rel|res|-h]"
  echo "  Configure a SOLEIL GRADES computer"
  echo "  The optional argument allows to specify which SOLEIL network should be configured"
  exit
fi

if [ "$(id -u)" -ne 0 ] ; then echo "Please run as root" ; exit 1 ; fi

NET=$1

# auto proxy config
if [ "x$NET" = "x" ]; then
  IP=`ip -o route get "8.8.8.8" 2>/dev/null | sed -e 's/^.* src \([^ ]*\) .*$/\1/'`
  case $IP in
      172*)
          NET=res
	  ;;
      195.221.4.*)
          NET=res
	  ;;
      195.221.*)
          NET=rel
	  ;;
  esac
fi

if [ "x$NET" = "x" ]; then
  NET=none
  echo "$0: WARNING: please specify the network (res,rel,none) as argument. Using $NET"
fi


# install the common part

($SCRIPT_DIR/../../config-soleil $NET)

###################
# configure fstab #
###################

FILE=/etc/fstab
echo "Saved fstab configuration in ${FILE}"
dd status=none of=${FILE} << EOF

# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a device; this may
# be used with UUID= as a more robust way to name devices that works even if
# disks are added and removed. See fstab(5).
#
# <file system>             <mount point>  <type>  <options>  <dump>  <pass>
UUID=3982-3E2F                            /boot/efi      vfat    defaults,noatime 0 2
UUID=268a1e1f-f088-4034-b4c4-2096ec6a9ab4 /              ext4    defaults,noatime,discard 0 1
UUID=c212cd3a-79f6-48af-bfab-b145a5aba1bf swap           swap    defaults,noatime,discard 0 0
tmpfs                                     /tmp           tmpfs   defaults,noatime,mode=1777 0 0
EOF

echo "Make sure $FILE content is what you need"
