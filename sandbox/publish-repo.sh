#!/bin/sh
#
# send repository packages to data-analysis=re-grades-02.exp
# the root pw at re-grades-02 is needed.

scp repository/*.deb root@re-grades-02.exp:/var/www/html/tmp/debian/dists/bookworm/main/binary-amd64/
ssh root@re-grades-02.exp update-deb-bookworm
