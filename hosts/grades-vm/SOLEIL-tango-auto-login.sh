#!/bin/sh

# Script for use with DARTS/qemu-web-desktop
# https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop
#   It is launched when a new session starts.
# The following symbols are replaced before execution:
#   @USER@ @VM@ @SESSION_NAME@ and @PW@
#
# Specify in /etc/qemu-web-desktop/config.pl
# my @config_scripts=('if("@VM@" =~ /debian/i): https://gitlab.com/soleil-data-treatment/infra-config/-/raw/master/hosts/grades-vm/SOLEIL-DARTS-auto-login.sh');
# $config{config_script}            = [@config_scripts];

# create local copy of script in /tmp
# r=`realpath $0`
# cp $r /tmp

# wait for network and LDAP to be ready
sleep 2

# get information from the LDAP: user exists ?

LDAP=ldap://195.221.10.1
USERFILE=/tmp/user.txt
user=@USER@
s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "mail=$user"`

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "cn=$user"`
fi

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "uid=$user"`
fi

# analyze the LDAP entry
echo "== Welcome to DARTS ! ==" > $USERFILE
date >> $USERFILE
echo " _____          _____ _______ _____ " >> $USERFILE
echo "|  __ \   /\   |  __ \__   __/ ____|" >> $USERFILE
echo "| |  | | /  \  | |__) | | | | (___  " >> $USERFILE
echo "| |  | |/ /\ \ |  _  /  | |  \___ \ " >> $USERFILE
echo "| |__| / ____ \| | \ \  | |  ____) |" >> $USERFILE
echo "|_____/_/    \_\_|  \_\ |_| |_____/ " >> $USERFILE
echo "https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop" >> $USERFILE
echo " " >> $USERFILE
echo "(c) Synchrotron SOLEIL / EXP / GRADES" >> $USERFILE
echo "https://data-analysis.synchrotron-soleil.fr" >> $USERFILE
echo "HELP: synchrotron-soleil-data-treatment@groupes.renater.fr" >> $USERFILE
echo " " >> $USERFILE
echo "-----------------------------------------------------" >> $USERFILE
echo "system=@VM@" >> $USERFILE
echo "user=$user" >> $USERFILE

shel=`echo $s | grep -o "loginShell: .* .*" | awk '{ print $2 }'`
if [ x$shel = 'x/dev/null' ]; then
  can_login=0;
else
  can_login=1;
  echo "canLogin=$can_login" >> $USERFILE
fi

# get email
email=`echo $s | grep -o "mail: .*" | awk '{ print $2 }'`
c=`echo $email | grep -o "@"`
if [ -z $c ]; then
  has_email=0;
else
  has_email=1;
  echo "email=$email" >> $USERFILE
fi

# get uid
uid=`echo $s | grep -o "uidNumber: .*" | awk '{ print $2 }'`
echo "uidNumber=$uid" >> $USERFILE

# get gid
gid=`echo $s | grep -o "gidNumber: .*" | awk '{ print $2 }'`
echo "gidNumber=$gid" >> $USERFILE

# get homedir
homedir=`echo $s | grep -o "homeDirectory: .*" | awk '{ print $2 }'`
echo "homeDirectory=$homedir" >> $USERFILE
echo "-----------------------------------------------------" >> $USERFILE
echo " " >> $USERFILE

cat $USERFILE

# set Firefox proxy to 0=none; 1=manual; 2=auto pac; 4=auto; 5=system.
echo 'pref("network.proxy.type", 5);' >> /etc/firefox/pref/soleil-prefs.js

# ------------------------------------------------------------------------------
user=tango-cs

# switch auto-login to USER if can_login and has_email, else remain as guest
# if [ $can_login = 1 -a $has_email = 1 ]; then
if [ $can_login = 1 ]; then

  # get PW and store it temporarily
  PWFILE=`mktemp -p /tmp`
  dd status=none of=${PWFILE} << 'EOF'
@PW@
EOF
  chown $user $PWFILE

  # map /home on host (allow Jupyter interaction)
  su -c "ssh-keygen -f ~/.ssh/known_hosts -R \"10.0.2.2\"" $user &
  su -c "mkdir -p /tmp/sshfs-home-host ; exit" $user
  ln -s /tmp/sshfs-home-host /mnt/home
  su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no @USER@@10.0.2.2:/home/ /tmp/sshfs-home-host ; exit" $user &

  echo "Add ~/Desktop/persistent_area"
  su -c "mkdir -p /tmp/sshfs-persistent_area; exit" $user
  ln -s /tmp/sshfs-persistent_area /mnt/persistent_area
  su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no @USER@@10.0.2.2:$homedir/ /tmp/sshfs-persistent_area ; exit" $user &
  echo "The persistent area is at: \$HOME/persistent_area (persistent, shared with the JupyterHub)" >> $USERFILE

  # Ruche mount
  echo "Add $homedir/Desktop/ruche"
  su -c "mkdir -p /tmp/sshfs-ruche; ln -s /tmp/sshfs-ruche \$HOME/Desktop/ruche ; exit" $user
  su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no @USER@@10.0.2.2:/nfs/ruche/ /tmp/sshfs-ruche ; exit" $user &
  mkdir -p /nfs
  ln -s /tmp/sshfs-ruche /nfs/ruche-sshfs
  ln -s /tmp/sshfs-ruche /mnt/ruche
  
  if [ -d "/nfs/ruche" ]; then
    # replace Ruche mount by the user sshfs mount which is more stable
    rm /nfs/ruche
  fi
  ln -s /nfs/ruche-sshfs /nfs/ruche
  
  # for the PhysAcc group (301)
  if [ "x$gid" = "x301" ]; then
    /sbin/iptables -I OUTPUT -d 195.221.10.41   -p tcp --dport ssh -j ACCEPT
    
    # for the PhysAcc group: mount 'home' and 'lustre/work' to sumo
    su -c "mkdir -p /tmp/sshfs-work-lustre ; exit" $user
    ln -s /tmp/sshfs-work-lustre /mnt/work-lustre
    su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no,ConnectTimeout=5 @USER@@195.221.10.24:/lustre/work/ /tmp/sshfs-work-lustre ; exit" $user &
    
    su -c "mkdir -p /tmp/sshfs-scratch-lustre ; exit" $user
    ln -s /tmp/sshfs-scratch-lustre /mnt/scratch-lustre
    su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no,ConnectTimeout=5 @USER@@195.221.10.24:/lustre/scratch/ /tmp/sshfs-scratch-lustre ; exit" $user &
    
    su -c "mkdir -p /tmp/sshfs-home-lustre ; exit" $user
    ln -s /tmp/sshfs-home-lustre /mnt/home-lustre
    su -c "cat $PWFILE | sshfs -o password_stdin -o StrictHostKeyChecking=no,ConnectTimeout=5 @USER@@195.221.10.24:/home/ /tmp/sshfs-home-lustre ; exit" $user &
    
    # add bookmarks
    su -c "mkdir -p \$HOME/.config/gtk-3.0; touch \$HOME/.config/gtk-3.0/bookmarks" $user
    for DIR in /tmp/sshfs-work-lustre /tmp/sshfs-scratch-lustre /tmp/sshfs-home-lustre ; do
      su -c "grep -qxF \"file://$DIR\" \$HOME/.config/gtk-3.0/bookmarks || echo \"file://$DIR\" >> \$HOME/.config/gtk-3.0/bookmarks" $user
    done
  fi
  rm $PWFILE

  RUCHEHOME=`echo "$homedir" | sed -e 's|/home|ruche|g'`
  echo "The Ruche is at:           \$HOME/ruche (for storing files permanently)" >> $USERFILE
  echo "  Your Ruche 'home' is at: \$HOME/$RUCHEHOME (your personnal persistent area)" >> $USERFILE
  echo "  The temporary area is:   \$HOME/ruche/share-temp (to exchange files)" >> $USERFILE
  
  echo " " >> $USERFILE
  
  su -c "cp $USERFILE \$HOME/Desktop/README" $user
  
  # add bookmarks
  su -c "mkdir -p \$HOME/.config/gtk-3.0; touch \$HOME/.config/gtk-3.0/bookmarks" $user
  for DIR in /tmp/sshfs-persistent_area /tmp/sshfs-home-host /tmp/sshfs-ruche ; do
    su -c "grep -qxF \"file://$DIR\" \$HOME/.config/gtk-3.0/bookmarks || echo \"file://$DIR\" >> \$HOME/.config/gtk-3.0/bookmarks" $user
  done
  
fi # can_login

# add firewall rules (/sbin//iptables) to restrict access to SOLEIL infrastructure machines

# allow SSH access to QEMU host (for sshfs to persistent area and /nfs/ruche)
#/sbin/iptables -I OUTPUT -d 195.221.10.46   -p tcp --dport ssh -j ACCEPT
#/sbin/iptables -I OUTPUT -d 195.221.10.56   -p tcp --dport ssh -j ACCEPT
#/sbin/iptables -I OUTPUT -d 195.221.10.24   -p tcp --dport ssh -j ACCEPT
/sbin/iptables -I OUTPUT -d 10.0.2.2        -p tcp --dport ssh -j ACCEPT

# restrict SSH (append at end -A) to all infrastructure servers
/sbin/iptables -A OUTPUT -d 195.221.0.0/16  -p tcp --dport ssh -j REJECT
/sbin/iptables -A OUTPUT -d 172.28.0.0/16   -p tcp --dport ssh -j REJECT
/sbin/iptables -A OUTPUT -d 192.168.0.0/16  -p tcp --dport ssh -j REJECT
/sbin/iptables -A OUTPUT -d 10.0.0.0/8      -p tcp --dport ssh -j REJECT

# save boot log for further analysis
mkdir -p /opt/infra-config
cp /var/log/syslog /opt/infra-config/boot.log || echo " "
chmod a+r /opt/infra-config/boot.log || echo " "
