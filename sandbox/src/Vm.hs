{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings         #-}

module Vm where

import           Data.Maybe
import           Data.Text
import           System.Directory
import           System.FilePath
import           System.Process.Typed

import           Prelude              hiding (unlines, writeFile)

import           Hosts

data Vm
  = Vm Distribution Flavour (Maybe Network) [Command]

setProxy :: Maybe Network -> [Command]
setProxy mr
  = case mr of
      Nothing -> []
      Just r -> [ CFileContent
                 ( FileContent "/etc/environment.d/20-soleil.conf" Nothing
                   [ comment
                   , "http_proxy=" <> url
                   , "https_proxy=" <> url
                   , "no_proxy=localhost"
                   ]
                 )
               ]
        where
          p@(Proxy comment _ _) = getProxy r
          url = getProxyUrl p

-- TODO only for wm ?

-- if [ `systemd-detect-virt` = "kvm" ]; then
--   echo 'CINNAMON_2D="true"' >> /etc/environment
-- fi

--------
-- Vm --
--------

data Command
    = CHostName HostName
    | TimeZone Text
    | CFileContent FileContent
    | RootPassword Text
    | FirstBootCommand Text
    | Run Text
    | RunCommand Text
    | Edit Text Text
    | MkDir Text
    | Install Script
    | Update

toCommandLines :: Command -> Script
toCommandLines (CHostName (HostName n)) = [ "hostname " <> n ]
toCommandLines (TimeZone tz) = [ "timezone " <> tz ]
toCommandLines (RootPassword p) = [ "root-password password:" <> p ]
toCommandLines (CFileContent (FileContent f _ (t:ts))) = ("write " <> f <> ":" <> t) : Prelude.map (\t' -> "append-line " <> f <> ":" <> t') ts
toCommandLines (CFileContent (FileContent _ _ [])) = []
toCommandLines (FirstBootCommand c) = [ "firstboot-command " <> c ]
toCommandLines (Run f) = [ "run " <> f ]
toCommandLines (RunCommand c) = [ "run-command " <> c ]
toCommandLines (Edit f c) = [ "edit " <> f <> ": " <> c ]
toCommandLines (MkDir f) = [ "mkdir " <> f ]
toCommandLines (Install ps) = [ "install " <> intercalate "," ps ]
toCommandLines Update = [ "update" ]

commandScript :: Vm -> Script
commandScript (Vm distribution _flavour mr extra) =
  let _version = getVersion distribution
  in Prelude.concatMap toCommandLines
     ([ CHostName (HostName "data-analysis")
      , TimeZone "Europe/Paris"
      , RootPassword "root"
      , FirstBootCommand "useradd -m -p \"\" -s /usr/bin/bash guest"
      , FirstBootCommand "adduser guest sudo"
      ]
      <>
      [ CFileContent $ sourceList distribution ]
      <>
      [ Run "install.sh"
      , Update
      , Install [ "gdm3", "gnome-core" ]
      , Edit "/etc/gdm3/daemon.conf" "s/#  AutomaticLoginEnable/AutomaticLoginEnable/"
      , Edit "/etc/gdm3/daemon.conf" "s/#  AutomaticLogin = user1/AutomaticLogin = guest/"
      , FirstBootCommand "systemctl stop gdm3"
      , FirstBootCommand "systemctl start gdm3"
      , CFileContent $ FileContent "/etc/dconf/profile/user" Nothing
        [ "user-db:user"
        , "system-db:local"]
      , MkDir "/etc/dconf/db/local.d"
      , CFileContent $ FileContent "/etc/dconf/db/local.d/00-app-menu" Nothing
        [ "[org/gnome/shell]"
        , "enabled-extensions=['apps-menu@gnome-shell-extensions.gcampax.github.com']"
        ]
      , FirstBootCommand "dconf update"
      ]
      <>
      extra
      <>
      setProxy mr)

getVmBaseUrl :: Distribution -> Url
getVmBaseUrl d@(Stable release) = "https://cloud.debian.org/images/cloud/" <> release <> "/latest/" <> getVersion d <> "-generic-amd64.qcow2"
getVmBaseUrl Testing = "https://cloud.debian.org/images/cloud/trixie/daily/latest/debian-13-generic-amd64-daily.qcow2"
getVmBaseUrl (Upgrade _ t) = getVmBaseUrl t

buildScript :: BuildType -> Vm -> FilePath -> Script
buildScript t (Vm distribution flavour _ _) isopath =
  build <> convert <> move t
  where
    version = getVersion distribution

    image = version <> "-" <> flavour

    buildVirtCustomize :: Url -> Script
    buildVirtCustomize url
      = [ "wget -O" <> image <> "-orig.qcow2" <> " " <> url
        , "qemu-img resize " <> image <> "-orig.qcow2 400G"
        , "cp " <> image <> "-orig.qcow2" <> " " <> image <> ".qcow2"
        , "virt-resize --expand /dev/sda1" <> " " <> image <> "-orig.qcow2" <> " " <> image <> ".qcow2"
        , "virt-customize -a " <> image <> ".qcow2 -v -x -attach " <> Data.Text.pack isopath <> " --commands-from-file " <> image <> ".commands"
        , "virt-sparsify --in-place " <> image <> ".qcow2"
        ]

    build :: Script
    build
      = ["echo \"Starting build\""]
        <>
        buildVirtCustomize (getVmBaseUrl distribution)

    convert :: Script
    convert
      = [ "echo \" Converting to VDI \""
        , "qemu-img convert -f qcow2 -O vdi " <> image <> ".qcow2" <> " " <> image <> ".vdi"
        ]

    move :: BuildType -> Script
    move t'
      = case t' of
          Local -> []
          Shm c -> [ "rm -rf " <> Data.Text.pack c <> "//" <> version
                  , "mkdir -p " <> Data.Text.pack c <> "//" <> version
                  , "mv * " <> Data.Text.pack c <> "//" <> version
                  ]

startScript :: Version -> Flavour -> Script
startScript v f
  = [ "#!/bin/sh"
    , ""
    , "qemu-system-x86_64 \\"
    , " -machine accel=kvm:tcg \\"
    , " -cpu host \\"
    , " -m 2048 \\"
    , " -snapshot \\"
    , " -drive file=" <> v <> "-" <> f <> ".qcow2,format=qcow2,if=virtio"
    ]

publishScript :: Version -> Flavour -> Script
publishScript v f
  = [ "#!/bin/sh"
    , ""
    , "scp " <> v <> "-" <> f <> ".vdi root@re-grades-02.exp:/var/www/html/tmp/machines/"
    ]

buildVm :: Vm -> IO ()
buildVm vm@(Vm distribution flavour _res _) = do
  curdir <- getCurrentDirectory
  let t = Shm (curdir </> "vm")  -- for now build on shm
  let version = getVersion distribution
  let builddir = (case t of
                    Local -> ""
                    Shm _ -> "/dev/shm/") ++ ("vm" </> unpack version)
  let isopath = "extra-packages.iso"
  let repository = "repository"

  -- set the current directory
  b <- doesDirectoryExist builddir
  case b of
    True -> do
      Prelude.putStr ("Build dir '" <> builddir <> "' already exist. Please remove it before proceeding\n")
      Prelude.putStr ("rm -rf " <> builddir <> "\n")
    False -> do
      -- create the build dir
      createDirectoryIfMissing True builddir

      withCurrentDirectory builddir $ do

        -- local repository or not
        r <- doesDirectoryExist repository
        case r of
          True -> do
            Prelude.putStr "Creating ISO\n"
            _ <- runProcess (proc "genisoimage" [ "-o", (builddir </> isopath)
                                               , "-R", "-J"
                                               , "-V", "EXTRA"
                                               , repository
                                               ])
            writeShell (builddir </> "install.sh")
              [ "mkdir /tmp/mount"
              , "mount LABEL=EXTRA /tmp/mount"
              , ""
              , "echo \"deb [allow-insecure=yes, trusted=yes] file:/tmp/mount ./\" > /etc/apt/sources.list.d/local.list"
              , "apt update"
              ]
          False -> do
            writeShell isopath []
            writeShell "install.sh" []

        -- create the command file
        writeShell (unpack (version <> "-" <> flavour <> ".commands")) (commandScript vm)

        -- create the start script
        writeShell "start.sh" (startScript version flavour)

        -- create the publish script
        writeShell "publish.sh" (publishScript version flavour)

        -- build the image
        writeShell "build.sh" (buildScript t vm isopath)
        _ <- runProcess (shell "./build.sh")

        return ()
