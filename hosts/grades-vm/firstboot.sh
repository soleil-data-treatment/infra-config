#!/bin/bash

# firstboot: Initiate a SOLEIL GRADES VM configuration, executed in the guest
# usage: `firstboot [options]` where options is a string with tokens:
#   res      for a ReS configuration
#   rel      for a REL configuration
#   none     for a no-proxy configuration (no apt is then possible inside SOLEIL)
#  <flavour> a flavour which is a directory inside 'software/flavours/' or 'hosts/'. 
#            package list files should start with 'package', and executable 
#            scripts are then executed in order. 00xx/config scripts are executed first.
#  <script>  any executable script that resides in the 'software' directory.
#
# available flavours are:
#   vm-bookworm  vm-bookworm-accelerator  vm-bookworm-ai   vm-soleil
#
# [options] can be combined as 'rel,bookworm,bookworm-ai' (no spaces allowed, separators: ,+)
#
# It is launched by systemd (virt-sysprep) after network.
# the symbol @ARG@ is replaced (given by config-vm).
#
# Example:
#   firstboot.sh rel,vm-bookworm,bin-xds.install

# - bootstrap
# - config-soleil (with proxy)
# - GRUB 1s
# - guest account, auto-login
# - background image
# - Ruche mounts
# - software install

export DEBIAN_FRONTEND=noninteractive 
export DEBCONF_NONINTERACTIVE_SEEN=true

# location of the infra-config (e.g. from git clone, copied by config-vm)
INFRA=/opt/infra-config

# check the Debian version

VERSION=`cat /etc/debian_version`
case $VERSION in
	11.*)
	DEBIAN_CODE="bullseye"
	;;
	12.*)
	DEBIAN_CODE="bookworm"
	;;
	*/sid)
	DEBIAN_CODE="unstable"
esac

ARG="$@"
if [ "x$ARG" = "x" ]; then
  ARG="@ARG@"
fi

case "$ARG" in
  -h|--help|help)
  echo "usage: '$0 [options]'"
  echo "where options is a string with tokens:"
  echo "  res      for a ReS configuration"
  echo "  rel      for a REL configuration"
  echo "  none     for a no-proxy configuration (no apt is then possible inside SOLEIL)"
  echo " <flavour> a flavour which is a directory inside 'software/flavours/' or 'hosts/'."
  echo "           package list files should start with 'package', and executable"
  echo "           scripts are then executed in order. 00xx/config scripts are executed first."
  echo " <script>  any executable script that resides in the 'software' directory."
  echo " "
  echo "available flavours are:"
  echo "  bookworm install extended packages for Debian 12 bookworm"
  echo "  soleil   install a limited list of scientific debs"
  echo " "
  echo "[options] can be combined as 'rel,bookworm' (no spaces allowed, separators: ,+)"
  echo " "
  echo "It is launched by systemd (virt-sysprep) after network."
  echo "the symbol @ARG@ is replaced (given by config-vm)."
  echo " "
  echo "Example:"
  echo "  $0 rel,bookworm,bin-xds.install"
  exit
  ;;
esac

echo "$0: Incoming arguments: $ARG"

# default: REL network/proxy conf, official software only
NET=rel

case "$ARG" in
  *rel*)
    NET=rel
  ;;
  *res*)
    NET=res
  ;;
  *none*)
    NET=
  ;;
esac
SOFTWARE="$ARG"

# Generic stuff for SOLEIL -----------------------------------------------------
if [ "x$NET" = "x" ]; then
  echo "No network configured (1st argument to $0 is empty). Launch manually ./config-soleil res or ./config-soleil rel."
  NET=none
fi

# bootstrap (proxy, apt, emacs, ssh, gpg) --------------------------------------
$INFRA/config-soleil-bootstrap $NET

# select window manager cinnamon
WM=cinnamon
# WM=gnome
sed -i s/WM=gnome/WM=$WM/g      $INFRA/config-soleil

# launch config for SOLEIL
$INFRA/config-soleil $NET

# import defined env var.
if [ -f /etc/environment ]; then
. /etc/environment
fi

# VM specific stuff (grub, guest account, backg image, config-soleil NET) ------
echo "INFO: Change GRUB with 1s time-out"
FILE=/etc/initramfs-tools/initramfs.conf
sed -i s/MODULES=most/MODULES=dep/g $FILE
update-initramfs -u

FILE=/etc/default/grub
sed -i s/GRUB_TIMEOUT=5/GRUB_TIMEOUT=1/g $FILE
#dd status=none of=${FILE} << EOF
#GRUB_DEFAULT=0
#GRUB_TIMEOUT=1
#GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
#GRUB_CMDLINE_LINUX_DEFAULT="quiet acpi=off"
#GRUB_CMDLINE_LINUX=""
#GRUB_DISABLE_OS_PROBER=true
#EOF
update-grub

# set keyboard
echo "INFO: Set keyboards for console (use e.g. setxkbmap fr or dpkg-reconfigure console-data or loadkeys fr)"
FILE=/etc/default/keyboard
dd status=none of=${FILE} << EOF
# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="fr,us"
XKBVARIANT="latin9,"
XKBOPTIONS=""

BACKSPACE="guess"
EOF

mkdir -p /etc/skel/Desktop
cp -r $INFRA/hosts/grades-vm/START-HERE /etc/skel/Desktop/
echo "This is a list of installed software in this environment:" > /etc/skel/Desktop/START-HERE/INSTALLED
date >> /etc/skel/Desktop/START-HERE/INSTALLED
echo "Configuration: $SOFTWARE" >> /etc/skel/Desktop/START-HERE/INSTALLED
echo "--------------------------------------------------------------------" >> /etc/skel/Desktop/START-HERE/INSTALLED

# add user guest
echo "INFO: Add guest account"
# pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
useradd -m -p "pai3Kc74gEZzM" "guest" -s /usr/bin/bash

echo "INFO: Set auto-login for 'guest'"
mkdir -p /etc/lightdm/lightdm.conf.d
FILE=/etc/lightdm/lightdm.conf.d/12-autologin.conf
dd status=none of=${FILE} << EOF
[SeatDefaults]
autologin-user=guest
autologin-user-timeout=0
EOF
mkdir -p /etc/lightdm/lightdm-gtk-greeter.conf.d/
FILE=/etc/lightdm/lightdm-gtk-greeter.conf.d/12-greeter.conf
dd status=none of=${FILE} << EOF
[greeter]
indicators = ~host;~spacer;~clock;~spacer;~language;~layout;~session;~a11y;~power
keyboard = onboard
a11y-states = keyboard
EOF

mkdir -p /etc/gdm3/
FILE=/etc/gdm3/custom.conf
dd status=none of=${FILE} << EOF
[daemon]
AutomaticLoginEnable=true
AutomaticLogin=guest
EOF

echo "INFO: Disable screen-lock"
mkdir -p /etc/dconf/db/local.d/
FILE=/etc/dconf/db/local.d/00-screensaver
dd status=none of=${FILE} << EOF
[org/gnome/desktop/screensaver]
lock-enabled=false
EOF

FILE=/etc/dconf/db/local.d/00-background
dd status=none of=${FILE} << EOF
# Specify the dconf path
[org/gnome/desktop/background]

# Specify the path to the desktop background image file
picture-uri='file://$INFRA/desktop-background-SOLEIL.png'

# Specify one of the rendering options for the background image:
picture-options='scaled'

# Specify the left or top color when drawing gradients, or the solid color
primary-color='000000'

# Specify the right or bottom color when drawing gradients
secondary-color='FFFFFF'

[org/cinnamon/desktop/background]

# Specify the path to the desktop background image file
picture-uri='file://$INFRA/desktop-background-SOLEIL.png'

# Specify one of the rendering options for the background image:
picture-options='scaled'

# Specify the left or top color when drawing gradients, or the solid color
primary-color='000000'

# Specify the right or bottom color when drawing gradients
secondary-color='FFFFFF'
EOF

dconf update

# ------------------------------------------------------------------------------
# install scientific software
echo "INFO: Setting software as '$SOFTWARE' (proxy=$http_proxy)"
echo "--------------------------------------------------------------------------"

function flavour_scripts() {
  DIR=$1
  if [ -f $DIR -a -x $DIR ]
  then
    # execute a single executable given as command line
    echo "INFO: Installing script $DIR"
    $DIR || echo "$DIR: FAILED/ERROR"
    echo "$DIR" >> /etc/skel/Desktop/START-HERE/INSTALLED
  elif [ -d $DIR ]
  then
    # first execute all 00-scripts and configure inside DIR
    for SCRIPT in $DIR/config* $DIR/00*
    do
      if [ -f $SCRIPT -a -x $SCRIPT ]
      then
        echo "INFO: Installing script $SCRIPT"
        $SCRIPT || echo "$SCRIPT: FAILED/ERROR"
        echo "$SCRIPT" >> /etc/skel/Desktop/START-HERE/INSTALLED
      fi
    done # 00xx scripts
    # search for 'package*' and execute with scientific-software.install
    for PKGS in $DIR/packages*
    do
      if [ -f $PKGS ]
      then
        echo "INFO: Installing packages $PKGS"
        # -k flag to keep pip
        $INFRA/software/scientific-software.install -k $PKGS || echo "$PKGS: FAILED/ERROR"
        cat $PKGS >> /etc/skel/Desktop/START-HERE/INSTALLED
      fi
    done # packages
    # execute all other scripts inside DIR
    for SCRIPT in $DIR/*
    do
      if [[ $SCRIPT == 00* ]]; then
        echo "INFO: Skipping script $SCRIPT (handled previously)."
      elif [ -f $SCRIPT -a -x $SCRIPT ]
      then
        echo "INFO: Installing flavour script $SCRIPT"
        $SCRIPT || echo "$SCRIPT: FAILED/ERROR"
        echo "$SCRIPT" >> /etc/skel/Desktop/START-HERE/INSTALLED
      fi
    done # scripts
  fi
}

# largest packages are: matlab mcrinstaller apbs-data rstudio chimera fiji axis2000 huygens aces3-data aces3 fonts-noto-* 

# extract the arguments from SOFTWARE (split ARG around ',' and '+')
IFS="+," read -ra my_array <<< $SOFTWARE

# loop around flavours when they exist
for FLAVOUR in "${my_array[@]}"
do
  echo "INFO: Setting flavour $FLAVOUR"
  if [ -d $INFRA/software/flavours/$FLAVOUR ]
  then
    flavour_scripts $INFRA/software/flavours/$FLAVOUR
    
  elif [ -d $INFRA/hosts/$FLAVOUR ]
  then
    flavour_scripts $INFRA/hosts/$FLAVOUR
    
  elif [ -f $INFRA/software/$FLAVOUR -a -x $INFRA/software/$FLAVOUR ]
  then
    flavour_scripts $INFRA/software/$FLAVOUR
    
  elif [ -f $FLAVOUR -a -x $FLAVOUR ]
  then
    flavour_scripts $FLAVOUR
  fi
done # tokens

echo "--------------------------------------------------------------------" >> /etc/skel/Desktop/START-HERE/INSTALLED
date >> /etc/skel/Desktop/START-HERE/INSTALLED
echo "DONE: $SOFTWARE" >> /etc/skel/Desktop/START-HERE/INSTALLED

# ------------------------------------------------------------------------------

echo "INFO: Install desktop background image"
su -c "gsettings set org.$WM.desktop.background picture-uri \"file:///usr/share/backgrounds/desktop-background-SOLEIL.png\"; exit" guest

echo "INFO: Set keyboard layouts"
su -c "gsettings set org.gnome.libgnomekbd.keyboard layouts \"['fr','fr\tmac','us','us\tmac']\"; exit" guest
su -c "gsettings set org.$WM.desktop.interface toolkit-accessibility true; exit" guest
su -c "gsettings set org.$WM.desktop.a11y.keyboard enable true; exit" guest
su -c "gsettings set org.gnome.desktop.a11y.applications screen-keyboard-enabled true; exit" guest
su -c "cp -r /etc/skel/Desktop/START-HERE ~/Desktop; exit" guest
su -c "gsettings set org.cinnamon enabled-applets \"['panel1:left:0:menu@cinnamon.org:0', 'panel1:left:1:show-desktop@cinnamon.org:1', 'panel1:left:2:grouped-window-list@cinnamon.org:2', 'panel1:right:0:systray@cinnamon.org:3', 'panel1:right:1:xapp-status@cinnamon.org:4', 'panel1:right:2:notifications@cinnamon.org:5', 'panel1:right:3:printers@cinnamon.org:6', 'panel1:right:4:removable-drives@cinnamon.org:7', 'panel1:right:6:favorites@cinnamon.org:9', 'panel1:right:7:network@cinnamon.org:10', 'panel1:right:8:sound@cinnamon.org:11', 'panel1:right:9:power@cinnamon.org:12', 'panel1:right:10:calendar@cinnamon.org:13', 'panel1:right:11:a11y@cinnamon.org:14', 'panel1:right:12:keyboard@cinnamon.org:15', 'panel1:right:14:trash@cinnamon.org:17']\"" guest
# this does not work as no X has started yet
# su -c "xkb-switch -s fr; exit" guest

# put back pip3
apt install -y -qq python3-pip
apt remove -y -qq collectd blueman fonts-noto-*
apt autoremove -y -qq
dpkg-reconfigure -f noninteractive openssh-server

# remove any left sudoers
echo "INFO: Remove sudoers"
for u in picca farhie bellachehab roudenko bac lambda
do
  bash -c "/sbin/deluser $u sudo"
done

# create local com-* accounts and disable them
systemctl stop nslcd
for u in com-ailes com-disco com-ode com-sirius com-anatomix com-flyscan com-pleiades com-sixs com-antares com-galaxies com-proxima1 com-slicing com-cassiopee com-hermes com-proxima2a com-smis com-cristal com-lucia com-psiche com-swing com-deimos com-mars com-puma com-tempo com-desirs com-metrologie com-rock com-detecteurs com-metrologie2 com-samba com-diffabs com-nanoscopium com-sextants
do
  /sbin/useradd -M $u
  /sbin/usermod -L $u
  /sbin/usermod -s /sbin/nologin $u
done

echo "INFO: Done installing network $NET and software $SOFTWARE."
echo "--------------------------------------------------------------------------"

# copy installation logs into infra-config
echo "INFO: Logs are in $INFRA"
if [ ! -f /var/log/kern.log ]
then
  journalctl -b -t kernel --no-pager > /var/log/kern.log
fi
if [ ! -f /var/log/syslog ]
then
  journalctl -b --no-pager > /var/log/syslog
fi
cp /var/log/syslog $INFRA/
cp /var/log/kern.log $INFRA/
cp /var/log/daemon.log $INFRA/
cp /var/log/boot.log $INFRA/
cp /var/log/messages $INFRA/

echo "INFO: FAILED packages"
grep firstboot $INFRA/syslog > $INFRA/install.log
grep -E 'FAILED|could not|unmet|ERROR|Abort|Conflicts|not installable|broken' $INFRA/install.log > $INFRA/failed.log
cat $INFRA/failed.log 

chmod a+r $INFRA/*log $INFRA/messages

sleep 1
# exit/shutdown
