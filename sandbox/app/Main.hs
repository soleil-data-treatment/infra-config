{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE OverloadedStrings         #-}

module Main where

import           Data.Maybe

import           Prelude    hiding (unlines, writeFile)

import           Hosts
import           Vm

-----------
-- Hosts --
-----------

mkHostRel :: HostName -> Distribution -> [Scriptable] -> Host
mkHostRel h d s = Host h d mr (c <> s)
  where
    mr = Just Rel
    c = [ script $ Raw "set -ex"
        , script $ Timezone "Europe/Paris"
        , script $ RmCruft
        , script $ Ntp d mr
        , script $ Ldap "ldap://195.221.10.1 ldap://195.221.10.2"
        , script $ Sudoers [ "picca", "farhie", "bellachehab", "roudenko", "bac" ]
        , script Exim4
        , script $ Firefox mr
        , script $ InstallPkgs [ "etckeeper", "unattended-upgrades" ]
        ]

mainHost :: IO ()
mainHost = do
  mapM_ buildHost hosts
    where
      hosts = [ mkHostRel (HostName "hermes5.exp.synchrotron-soleil.fr") (Stable "bookworm") [ script $ InstallPkgs [ "pynx/bookworm-backports"] ]
              , mkHostRel (HostName "sixs7.exp.synchrotron-soleil.fr") (Stable "trixie") [ script $ Gnome
                                                                                         , script $ Ruche Sixs
                                                                                         , script $ InstallPkgs [ "binoculars", "ghkl", "silx" ]
                                                                                         ]
              ]

--------
-- Vm --
--------

  -- let install = let installBookworm = [ "ase"
  --                                     , "bornagain"
  --                                     , "fontconfig-config"
  --                                     , "gdm3"
  --                                     , "gedit"
  --                                     , "gnome-core"
  --                                     , "pyfai"
  --                                     , "pymca"
  --                                     , "python3-natsort"
  --                                     , "python3-tables"
  --                                     , "python3-xrayutilities"
  --                                     , "silx"
  --                                     , "spyder"
  --                                     , "sudo"
  --                                     , "systemd-timesyncd"
  --                                     ]
  --                   installBookwormBackports = [ "libhkl5"
  --                                              , "binoculars"
  --                                              , "ghkl"
  --                                              ]
  --               in case distribution of
  --                    Stable release -> Install (installBookworm
  --                                              <> Prelude.map ( <> ("/" <> release <> "-backports")) installBookwormBackports )
  --                    Testing -> Install (installBookworm <> installBookwormBackports)

vmTesting :: Vm
vmTesting = Vm Testing "vm-debian13" (Just Rel)
  [ Install [ "devscripts"
            , "dgit"
            , "emacs-nox"
            , "git"
            , "git-gui"
            , "gitk"
            , "jupyter"
            , "jupyterlab"
            , "jupyter-notebook"
            , "node-jupyter-widgets-base-manager"
            , "node-jupyter-widgets-controls"
            , "node-jupyter-widgets-html-manager"
            , "python3-ipywidgets"
            ]
  , RunCommand "apt-get -q -y -o Dpkg::Options::=--force-confnew build-dep ipywidgets"
  ]

mainVm :: IO ()
mainVm = do
  mapM_ buildVm [ vmTesting ]
--    [ Vm (Stable "bookworm") "sixs" Rel
--    , Vm Testing "sixs" Rel
--    ]

main :: IO ()
main = do
  mainHost
--  mainVm
