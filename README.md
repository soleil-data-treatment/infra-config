# infra-config

A tool to configure machines and virtual machines.

:warning: beware this project is designed for the Soleil Synchrotron Radiation Infrastructure.

Configure a computer
--------------------

Login on the computer with a minimalistic system, then issue:
- `git clone http://gitlab.com/soleil-data-treatment/infra-config`
- `cd infra-config`
- `./config-soleil-bootstrap`

or, to manually set the SOLEIL network to configure:

- `./config-soleil-bootstrap res`
- `./config-soleil-bootstrap rel`

This sets the initial configuration in order to work at SOLEIL.
Then, you may issue e.g.

- `$ hosts/<hostname>/configure`

or directly e.g.

- `$ ./config-soleil res`
- `$ ./config-soleil rel`

Install a software
------------------

After the configure is done, you can select specific software to install
- `ls software/*.install`

Then, after choosing what you need:
- execute the <software>.install script to install the software
- execute the <software>.config script to configure the service if relevant.

We recommend to use at least the script:

- `software/scientific-software.install`

which takes as input a file name containing a list (rows) of packages to install, or a list of packages (string), or URLs of `deb` packages (string).

A typical `packages` file would look like:
```
# packages file
pkg1.deb
pkg2.deb
# remove package
-pkg1
# remote deb
http://path/to/deb
https://path/to/deb
ftp://path/to/deb
file://path/to/deb
```

Configure a virtual machine
---------------------------

### Flavours

We provide prepared flavours which are a collection of package lists and installation/configuration scripts. They are often links to e.g. the `software` or the `hosts` directory.

The flavour elements are installed in the following order:

- Executable scripts that start with `config`
- Executable scripts that start with `00`
- Package lists in alphabetic order
- Other executable scripts in alphabetic order

For instance, the `vm-bookworm-ai` flavour contains currently (as links):
```
jupyter-terminal-fix.install  pip3-conda-alphafold.install
packages-bookworm-ai          pip3-tensorflow-ai.install
```

### Using the `Makefile`

To generate a VM, just type, from the project root (using default `VM_FLAVOUR=rel,vm-soleil`):

- `make vm`

You may customize the target VM content with e.g.

- `make vm VM_FLAVOUR=rel,vm-bookworm,matlab.install`

where `VM_FLAVOUR` is a list of flavours (in `hosts` or `software/flavours`), package lists and installation/configuration scripts, which all reside in the `software` directory.

The final VM file is created in the /tmp directory, but you may specify the target file with `VM_NAME`, e.g.:

- `make vm VM_NAME=/tmp/debian-target.qcow2`

### Using the `hosts/grades-vm/config-vm` script

It is possible to automatically generate virtual machine images (e.g. VMDK, VDI, QCOW2) by mean of the `config-vm` script.

The `config-vm` command performs the following tasks:
- when not given, get a a default image from e.g. https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2 (option `-i DISK`).
- resize the image with a given size (e.g. 400GB by default, option `-d SIZE`).
- specify a target VM image file (default uses date, option `-o OUT`).
- execute a set of `virt-customize` commands (option `-c CMDS`). Such commands may be to install packages, copy/edit files, etc. See https://libguestfs.org/virt-customize.1.html
- insert a script to execute at first boot of the VM (option `-f FIRSTBOOT`).
- the VM is optionally started, and all bits are executed (option `-s`)

To have this tool enabled, the following packages should be installed:
- sudo apt install libguestfs-tools qemu qemu-utils

A typical command would be:
- `hosts/grades-vm/config-vm -s -i disk.qcow2 -c commands_from_file -f firstboot.sh res,vm-soleil`
- `hosts/grades-vm/config-vm -s -i stable -o /dev/shm/debian-stable-pip.qcow2        rel,vm-soleil`

or simply (for a minimal VM with _res_ SOLEIL network):
- `hosts/grades-vm/config-vm -s res`

The additional arguments (here `res`) are passed to the CMDS and FIRSTBOOT scripts below as the `@ARG@` symbol which is replaced therein. Currently the following options are handled:

-  res      for a ReS configuration
-  rel      for a REL configuration
-  none     for a no-proxy configuration (no apt is then possible inside SOLEIL)
- `<flavour>` a flavour which is a directory inside 'software/flavours/'. package list files starting with 'package' are first installed,  and executable scripts are then executed in order.
- `<script>`  any executable script that resides in the 'software' directory.

Options can be combined as a single word using `,` as separators, e.g. 
- `hosts/grades-vm/config-vm ... rel,vm-bookworm,bin-xds.install`

The process generates a new disk image, and prints the command to execute to start it, e.g.:
- `qemu-system-x86_64  -m 4096 -smp 4 -device ich9-ahci,id=ahci -enable-kvm -cpu host -vga qxl -netdev user,id=mynet0 -device virtio-net,netdev=mynet0 -device virtio-balloon -hda <disk-date>.qcow2`
- `./start <disk-date>.qcow2`

It is further possible to compress the final qcow2 image with e.g.:
- `(qemu-img convert -O qcow2 -c debian-stable-pip.qcow2 debian-stable-pip-small.qcow2; rm debian-stable-pip.qcow2)`

The whole process execution time is about 5-10 minutes for a minimal set-up, and about 1-2 hour for a complete set-up (depends on the number of software to install).

It is possible, upon generation, to change the network settings with:
- `sudo /opt/infra-config/config-soleil res`
- `sudo /opt/infra-config/config-soleil rel`

which allows to launch only one VM generation, and perform a quick network re-assignment.

The files you can edit to tune the configuration of the VM are the following:

| File | Scope |
|------|-------|
| `config-soleil-bootstrap {res,rel}` | bootstrap for given NET (initial script, proxy, apt, emacs, ssh, gpg) |
| `hosts/grades-vm/firstboot.sh {res,rel},{flavour},{exec.install|config}` | VM specific (grub, guest account, backg image, config-soleil NET with LDAP). The software stack can be `{base,reduced,full,pip}` |
| `software/scientific-software.install {packages,URL's}` | install scientific software (list of packages) |



