#!/bin/bash

# Actions:
# - Install packages via config-soleil
# - /etc/fstab

# configure: Initiate a SOLEIL GRADES computer configuration (minimal stuff)
#            This script mainly calls `config-soleil` and sets FSTAB.
# usage:
#  `configure`      for an automatic proxy setting (based on IP)
#  `configure res`  for a ReS configuration
#  `configure rel`  for a REL configuration
#  `configure none` for a no-proxy configuration

set -e
SCRIPT_DIR=$(cd `dirname $0` && pwd)

if [ "x$1" = "x-h" ]; then
  echo "$0 [rel|res|-h]"
  echo "  Configure a SOLEIL GRADES computer"
  echo "  The optional argument allows to specify which SOLEIL network should be configured"
  exit
fi

if [ "$(id -u)" -ne 0 ] ; then echo "Please run as root" ; exit 1 ; fi

NET=$1

# auto proxy config
if [ "x$NET" = "x" ]; then
  IP=`ip -o route get "8.8.8.8" 2>/dev/null | sed -e 's/^.* src \([^ ]*\) .*$/\1/'`
  case $IP in
      172*)
          NET=res
	  ;;
      195.221.4.*)
          NET=res
	  ;;
      195.221.*)
          NET=rel
	  ;;
  esac
fi

if [ "x$NET" = "x" ]; then
  NET=none
  echo "$0: WARNING: please specify the network (res,rel,none) as argument. Using $NET"
fi


# install the common part

($SCRIPT_DIR/../../config-soleil $NET)

###################
# configure fstab #
###################

FILE=/etc/fstab
echo "Saved fstab configuration in ${FILE}"
dd status=none of=${FILE} << EOF

# DO NOT EDIT, MANAGED VIA PROPELLOR
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=1e528acf-4dec-450b-9178-37f5c769f260 /               ext4    errors=remount-ro 0       1
# swap was on /dev/sda5 during installation
UUID=357161c8-8941-4de7-95d1-e8eaf42a98b5 none            swap    sw              0       0
/dev/sdb1    /srv    ext4    defaults    0    2
ruche-diffabs.exp.synchrotron-soleil.fr:/diffabs-users    /nfs/ruche-diffabs/diffabs-users    nfs    rw,intr,hard,acdirmin=1,vers=3,local_lock=flock,x-systemd.automount    0    2
ruche-diffabs.exp.synchrotron-soleil.fr:/diffabs-soleil    /nfs/ruche-diffabs/diffabs-soleil    nfs    rw,intr,hard,acdirmin=1,vers=3,local_lock=flock,x-systemd.automount    0    2
EOF

echo "Make sure $FILE content is what you need"

#####################
# sbuild and chroot #
#####################

PROXY_URL=http://195.221.10.6:8080

. "$SCRIPT_DIR"/../../software/sbuild.lib
sbuild_install $PROXY_URL

sbuild_chroot "unstable"
sbuild_chroot "bookworm"
